import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'datePipe'
})
export class DatePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value == null) {
      return '';
    } else {
      if (value.length > 0) {
        const convert_date = value.split('-');
        return convert_date[2] + '-' + convert_date[1] + '-' + convert_date[0];
      } else {
        return '';
      }
    }
  }

}
