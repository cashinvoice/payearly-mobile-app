import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicDiscountDetailsComponent } from './dynamic-discount-details.component';

describe('DynamicDiscountDetailsComponent', () => {
  let component: DynamicDiscountDetailsComponent;
  let fixture: ComponentFixture<DynamicDiscountDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicDiscountDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicDiscountDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
