import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService } from '../../_service/authentication.service';
import { GlobalFunctionService } from '../../_service/global-function.service';
import { CommonService } from '../../_service/common.service';
import { SharedService } from '../../_service/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

import { environment } from '../../../environments/environment';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { of } from 'rxjs';


declare var $: any;
declare var jQuery: any;
declare const toastbox: any;


@Component({
  selector: 'app-dynamic-discount-details',
  templateUrl: './dynamic-discount-details.component.html',
  styleUrls: ['./dynamic-discount-details.component.css']
})
export class DynamicDiscountDetailsComponent implements OnInit {

  product_id  = localStorage.getItem('product_id');
  settlement_date = localStorage.getItem('settlement_date');//disb_date

  details = [];
  authorizeAPI = environment.API_BUYER_SELLER_CHECKER_AUTHORIZED_UPDATE;
  transaction_id: number;
  screen_api: string;
  list_status: boolean = false;
  cur_date = '';
  discPerc = 0;


  constructor(
    private router: Router,
    private auth: AuthenticationService,
    private global: GlobalFunctionService,
    private common: CommonService,
    private shared: SharedService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService) { }

    ngOnInit() {
    // $("#loader").hide(250);
      this.shared.clearSearchData();
      this.shared.dynamicsharedItem.subscribe(item => this.details = item);
      this.transaction_id = this.details[0].id;
      this.cur_date = this.details[1];
      this.discPerc = this.details[2];
      this.screen_api = environment.API_TRANSACTION_DETAILS + `?trnx_id=${this.transaction_id}`;
      this.getInfo(this.screen_api);
    }

    ngOnDestroy () {
      document.body.className = "";
    }
    getInfo(urlPath) {
      $("#loader").show(250);
      this.list_status = false;
      this.common.getInfo(this.screen_api).pipe(first()).subscribe((res: any) => {
        if(res.result.success) {
          const response = res.data.invoice_details;
          this.details[0].buyer_gstn = response[0].buyer_gstn;
          this.details[0].seller_gstn = response[0].seller_gstn;
          this.details[0].invoice_adjustments = response[0].invoice_adjustments;
  
          this.details[0].invoice_net_amt = response[0].invoice_net_amt;
          this.details[0].invoice_acceptance_date = response[0].invoice_acceptance_date;
          this.details[0].buyer_ref_no = response[0].buyer_ref_no;
          this.details[0].po_number = response[0].po_number;
          this.details[0].disbursed_date = response[0].disbursed_date;
          }
        this.list_status = true;
        $("#loader").hide(250);
      }, error => { localStorage.clear(); location.reload(); });
    }

    goToList() {
      this.details = [];
      this.details.pop();
      this.router.navigate(['/dynamicdiscount']);
    }

    authorized(item){
      if (!jQuery('#customCheck7').is(':checked')) {
        // this.global.showError('Accept T&C');
        toastbox("accepttandc", 2000);
        return false;
      }
        let send_obj: any = {};
        let newItem : any = {};
        let itemArr: any = [];
        newItem.buyer_id = item.buyer_id;
        newItem.seller_id = item.seller_id;
        newItem.product_id = this.product_id;
        newItem.id = item.id;
        newItem.status = item.status;
        newItem.invoice_due_date = item.invoice_due_date;
        newItem.disb_date = this.settlement_date;
        newItem.disb_amt = item.net_payable_amt;
        newItem.disc_amt = item.disc_amt;
        newItem.agreed_discount_percentage = item.agreed_discount_percentage;
        itemArr.push(newItem);
        send_obj.transactions = itemArr;
        send_obj.type = "update";
        send_obj.postDate = this.cur_date;
        send_obj.categoryType = "dynamic";
        // send_obj.postDate = this.cur_date;
        send_obj.discPerc = this.discPerc;
        this.common.putInfo(this.authorizeAPI, send_obj).pipe(first()).subscribe((res: any) => {
          // this.model_filter_status = false;
          // this.global.showSuccess('Authorized successfully submitted.')
          // this.getInfo(this.screen_api);
          if(res.result.success){
            toastbox('toast-authorized-success', 5000);
            setTimeout(() => {
              this.shared.clearDynamicDiscData();
              window.history.back();
            }, 500);
          }
          else{
            toastbox('toast-authorization-failure', 5000);
          }
    });
    }

}
