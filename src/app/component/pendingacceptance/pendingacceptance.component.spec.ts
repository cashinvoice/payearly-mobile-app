import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingacceptanceComponent } from './pendingacceptance.component';

describe('PendingacceptanceComponent', () => {
  let component: PendingacceptanceComponent;
  let fixture: ComponentFixture<PendingacceptanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingacceptanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingacceptanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
