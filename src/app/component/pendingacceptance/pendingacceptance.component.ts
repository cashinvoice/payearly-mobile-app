import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "../../_service/authentication.service";
import { GlobalFunctionService } from "../../_service/global-function.service";
import { CommonService } from "../../_service/common.service";
import { SharedService } from "../../_service/shared.service";

import { NgxSpinnerService } from "ngx-spinner";

import { environment } from "../../../environments/environment";
import { first } from "rxjs/operators";
import { Router } from "@angular/router";
import {
  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  ValidatorFn,
} from "@angular/forms";
import { of } from "rxjs";

declare var $: any;
declare var jQuery: any;
declare const toastbox: any;

@Component({
  selector: "app-pendingacceptance",
  templateUrl: "./pendingacceptance.component.html",
  styleUrls: ["./pendingacceptance.component.css"],
})
export class PendingacceptanceComponent implements OnInit {
  header_title = "";
  entity_id = localStorage.getItem("entity_id");
  is_financier = localStorage.getItem("is_financier");
  settlement_date = localStorage.getItem("settlement_date");
  name = localStorage.getItem("name");
  penAcc_data: any = [];
  roleName = localStorage.getItem("role_name");
  product_id = localStorage.getItem("product_id");

  select_all = [];
  bulk_upload_status = false;
  primary_key = "id";
  data_length = 0;
  unique_key = crypto.getRandomValues(new Uint16Array(1))[0]*2;
  card_data = [];
  screen_api = "";

  showDetails = false;
  details: any = [];
  list = true;
  listStatus = false;
  authorizeAPI = environment.API_BUYER_SELLER_CHECKER_AUTHORIZED_UPDATE;
  pageLimit = environment.PAGE_LIMIT;
  p = 1;
  totalItems;
  tbl_pagination;
  pagination_length = true;
  cur_date = '';


  total_invoice_amount = 0;
  total_discount_amount = 0;
  total_net_amount = 0;

  temp_item: any = {};

  constructor(
    private router: Router,
    private auth: AuthenticationService,
    private global: GlobalFunctionService,
    private common: CommonService,
    private shared: SharedService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService
  ) {
    this.auth.checklogin();
  }

  ngOnInit() {
    document.body.className = "backgroundImg";
    this.shared.sharedItem.subscribe(item => this.details = item);
    if (!this.penAcc_data) {
      this.data_length = 0;
    } else {
      this.data_length = this.penAcc_data.length;
    }
    let urlpath = environment.API_BUYER_SELLER_CHECKER_AUTHORIZED;
    const body = document.getElementsByTagName("body")[0];
    body.classList.remove("login");
    body.classList.add("nav-md");
    const url_name = this.router.url;
    this.header_title = url_name.replace("/", "");
    this.global.setTitle(this.header_title);

    this.screen_api =
      environment.API_BUYER_SELLER_CHECKER_AUTHORIZED +
      `?entity_id=${this.entity_id}&settlement_date=${this.settlement_date}&npp=${environment.PAGE_LIMIT}`;

    this.getInfo(this.screen_api);
  }

  ngOnDestroy() {
    document.body.className = "";
  }

  getInfo(api) {
    $("#loader").show(250);
    this.total_invoice_amount = 0;
    this.total_discount_amount = 0;
    this.total_net_amount = 0;
    this.common
      .getInfo(api)
      .pipe(first())
      .subscribe(
        (res: any) => {
          if (res.result.success) {
            const response = res.data;
            this.penAcc_data = response.results;
            this.card_data = response.cards;
            this.cur_date = response.cur_date;

            this.data_length = this.penAcc_data.length;
            const pagination = res.data.pagination;
            this.tbl_pagination = pagination;
            this.p = pagination.current;
            this.totalItems = pagination.total;
            this.total_invoice_amount += Number(this.card_data[0].total_amt);
            this.total_discount_amount += Number(this.card_data[0].disc_amt);
            this.total_net_amount += Number(this.card_data[0].total_amt - this.card_data[0].disc_amt);

            if (pagination.total <= 10) {
              //make it 10
              this.pagination_length = false;
            }
            // this.totalItems = 3;
          }
          $("#loader").hide(250);
        },
        (error) => {
          localStorage.clear();
          location.reload();
        }
      );
  }

  selectAll(event) {
    const tableList = JSON.parse(JSON.stringify(this.penAcc_data));
    this.select_all = [];
    if (event.target.checked == true) {
      this.total_invoice_amount = 0;
      this.total_discount_amount = 0;
      this.total_net_amount = 0;
      let i;
      for (i = 0; i < tableList.length; i++) {
        const rowUniqueId =
          "#action-checkbox-" + this.setPrimaryKey(tableList[i]);
        jQuery(rowUniqueId).prop("checked", true);
        const rowStatus = jQuery(rowUniqueId).prop("checked");
        if (rowStatus) {
          let send_obj: any = {};
          let newItem: any = {};
          let itemArr: any = [];
          newItem.buyer_id = tableList[i].buyer_id;
          newItem.seller_id = tableList[i].seller_id;
          newItem.product_id = this.product_id;
          newItem.id = tableList[i].id;
          newItem.status = tableList[i].status;
          newItem.invoice_due_date = tableList[i].invoice_due_date;
          newItem.disb_date = this.settlement_date;
          newItem.disb_amt = tableList[i].net_payable_amt;
          newItem.disc_amt = tableList[i].disc_amt;
          newItem.agreed_discount_percentage =
            tableList[i].agreed_discount_percentage;

          this.total_invoice_amount += Number(tableList[i].inv_amt);
          this.total_discount_amount += Number(tableList[i].disc_amt);
          this.total_net_amount += Number(tableList[i].net_payable_amt);
          // itemArr.push(newItem);
          // send_obj.transactions = itemArr;
          this.select_all.push(newItem);
          // this.select_all.push(tableList[i]);
        }
      }
    } else {
      this.select_all = [];
      this.total_invoice_amount = Number(this.card_data[0].total_amt);
      this.total_discount_amount = Number(this.card_data[0].disc_amt);
      this.total_net_amount = Number(this.card_data[0].total_amt - this.card_data[0].disc_amt);
      jQuery(".tbody_check_content").prop("checked", false);
    }
    this.applyScript();
  }

  single_select(event, rowObject: Object) {
    const rowData = JSON.parse(JSON.stringify(rowObject));
    if (event.target.checked === true) {
      const temp_child: any = {};
      const rowUniqueId = "#action-checkbox-" + this.setPrimaryKey(rowData);
      $(rowUniqueId).prop("checked", true);
      let send_obj: any = {};
      let newItem: any = {};
      let itemArr: any = [];
      newItem.buyer_id = rowData.buyer_id;
      newItem.seller_id = rowData.seller_id;
      newItem.product_id = this.product_id;
      newItem.id = rowData.id;
      newItem.status = rowData.status;
      newItem.invoice_due_date = rowData.invoice_due_date;
      newItem.disb_date = this.settlement_date;
      newItem.disb_amt = rowData.net_payable_amt;
      newItem.disc_amt = rowData.disc_amt;
      newItem.agreed_discount_percentage = rowData.agreed_discount_percentage;
      itemArr.push(newItem);
      send_obj.transactions = itemArr;
      this.select_all.push(newItem);
      if (this.select_all.length == 1) {
        this.total_invoice_amount = 0;
        this.total_discount_amount = 0;
        this.total_net_amount = 0;
      }

      this.total_invoice_amount += Number(rowData.inv_amt);
      this.total_discount_amount += Number(rowData.disc_amt);
      this.total_net_amount += Number(rowData.net_payable_amt);
    } else {

      const uniqueId = this.setPrimaryKey(rowData);
      const itemToDelete = [uniqueId];
      for (let i = 0; i < this.select_all.length; i++) {
        const obj = this.select_all[i];
        const removeId = this.setPrimaryKey(obj);
        if (itemToDelete.indexOf(removeId) !== -1) {
          this.total_invoice_amount -= Number(rowData.inv_amt);
          this.total_discount_amount -= Number(obj.disc_amt);
          this.total_net_amount -= Number(obj.disb_amt);
          this.select_all.splice(i, 1);
          $("#action-checkbox-" + removeId).prop("checked", false);
          if(this.select_all.length == 0){
          }
        }
        if (this.select_all.length == 0) {
          this.total_invoice_amount += Number(this.card_data[0].total_amt);
          this.total_discount_amount += Number(this.card_data[0].disc_amt);
          this.total_net_amount += Number(this.card_data[0].total_amt - this.card_data[0].disc_amt);
        }
      }
    }
    this.applyScript();
  }

  setPrimaryKey(objectData: object) {
    for (const key of Object.keys(objectData)) {
      if (key === this.primary_key) {
        return objectData[key];
      }
    }
  }

  applyScript() {
    if (this.select_all.length > 1) {
      // jQuery('.footer_pagination_' + this.unique_key).css('pointer-events', 'none');
      this.bulk_upload_status = true;
    } else {
      // jQuery('.footer_pagination_' + this.unique_key).css('pointer-events', 'unset');
      this.bulk_upload_status = false;
    }
  }

  getDetailedData(item) {
    this.select_all = [];
    this.details = [];
    this.details.push(item);
    this.details.push(this.cur_date);
    this.list = false;
    this.showDetails = true;
    this.listStatus = true;
    this.shared.nextItem(this.details);
    this.router.navigate(["/pendingacceptancedetails"]);
  }

  clearSelect() {
    this.select_all = [];
    this.temp_item = {};
    jQuery(".tbody_check_content").prop("checked", false);
  }

  authorized() {

    if (!jQuery('#customCheck2').is(':checked')) {
      // this.global.showError('Accept T&C');
      toastbox("accepttandc", 2000);
      return false;
    }
    let send_obj: any = {};
    send_obj.transactions = this.select_all;
    send_obj.type = "update";
    send_obj.postDate = this.cur_date;
    this.common
      .putInfo(this.authorizeAPI, send_obj)
      .pipe(first())
      .subscribe((res: any) => {
        // this.model_filter_status = false;
        // this.global.showSuccess('Authorized successfully submitted.')
        // this.getInfo(this.screen_api);
        if (res.result.success) {
          toastbox("toast-bulkauthorized-success", 5000);
          setTimeout(() => {
            window.location.reload();
          }, 500);
        } else {
          toastbox("toast-bulkauthorization-failure", 5000);
          setTimeout(() => {
            window.location.reload();
          }, 500);
        }
      });
  }

  authorizeSingle(item) {

    if (!jQuery('#customCheck3').is(':checked')) {
      // this.global.showError('Accept T&C');
      toastbox("accepttandc", 2000);
      return false;
    }

    let send_obj: any = {};
    let newItem: any = {};
    let itemArr: any = [];
    newItem.buyer_id = item.buyer_id;
    newItem.seller_id = item.seller_id;
    newItem.product_id = this.product_id;
    newItem.id = item.id;
    newItem.status = item.status;
    newItem.invoice_due_date = item.invoice_due_date;
    newItem.disb_date = this.settlement_date;
    newItem.disb_amt = item.net_payable_amt;
    newItem.disc_amt = item.disc_amt;
    newItem.agreed_discount_percentage = item.agreed_discount_percentage;
    itemArr.push(newItem);
    send_obj.transactions = itemArr;
    send_obj.type = "update";
    send_obj.posrDate = this.cur_date;
    this.common
      .putInfo(this.authorizeAPI, send_obj)
      .pipe(first())
      .subscribe((res: any) => {
        // this.model_filter_status = false;
        // this.global.showSuccess('Authorized successfully submitted.')
        // this.getInfo(this.screen_api);
        if (res.result.success) {
          toastbox("toast-authorized-success", 5000);
          setTimeout(() => {
            // window.history.back();
          }, 500);
          setTimeout(() => {
            window.location.reload();
          }, 500);
        } else {
          toastbox("toast-authorization-failure", 5000);
        }
      });
    this.select_all = [];
    this.temp_item = {};
    jQuery(".tbody_check_content").prop("checked", false);
  }

  pushItem(event, item) {
    // event.stopPropagation();
    this.temp_item = item;
    setTimeout(function () {
      $("#DialogForSingle").modal("show");
    }, 0);

    event.stopPropagation();
  }

  getPage(page: number) {

    $("#loader").show(250);
    this.p = page;
    this.getPageCounter({ currentObj: page, pageLimit: this.pageLimit });
  }

  pageCounter(page, num) {
    if (page > 0) {
      let current = page - 1;
      current = current * this.pageLimit + num;
      return current;
    } else {
      return num;
    }
  }

  showLast(start, length) {
    return start + (length - 1);
  }


  getPageCounter(pageObject: any) {
    $("#loader").show(250);
    const pagePath = this.screen_api + `&page=${pageObject.currentObj}`;
    this.getInfo(pagePath);
  }
}
