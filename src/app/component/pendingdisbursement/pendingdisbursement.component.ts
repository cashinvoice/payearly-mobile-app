import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_service/authentication.service';
import { GlobalFunctionService } from '../../_service/global-function.service';
import { CommonService } from '../../_service/common.service';
import { SharedService } from '../../_service/shared.service';

import { NgxSpinnerService } from 'ngx-spinner';

import { environment } from '../../../environments/environment';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { of } from 'rxjs';

declare var $: any;
declare var jQuery: any;
declare const toastbox: any;

@Component({
  selector: 'app-pendingdisbursement',
  templateUrl: './pendingdisbursement.component.html',
  styleUrls: ['./pendingdisbursement.component.css']
})
export class PendingdisbursementComponent implements OnInit {
  header_title = '';
  entityId = localStorage.getItem('entity_id');
  is_financier = localStorage.getItem('is_financier');
  settlement_date = localStorage.getItem('settlement_date');
  name = localStorage.getItem('name');
  roleName = localStorage.getItem('role_name');
  primary_key = 'id';
  card_data = [];
  list_status = false;
  screen_api: string;
  data_length =0;
  disb_data: any = [];
  // details = [];
  showDetails = false;
  details: any = [];
  list = true;
  listStatus = false;
  pageLimit = environment.PAGE_LIMIT;
  p = 1;
  totalItems;
  tbl_pagination;
  pagination_length = true;



  constructor(private router: Router, private auth: AuthenticationService, private global: GlobalFunctionService, private common: CommonService, private shared: SharedService, private fb: FormBuilder, private spinner: NgxSpinnerService) {
    this.auth.checklogin();
  }

  ngOnInit() {
    document.body.className = "backgroundImg";
    this.shared.disbsharedItem.subscribe(item => this.details = item);
    if(!this.disb_data) {
      this.data_length = 0;
    } else {
      this.data_length = this.disb_data.length;
    }
    let url_name = this.router.url;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('login');
    body.classList.add('nav-md');
    this.header_title = url_name.replace('/', '');
    this.global.setTitle(this.header_title);
    this.screen_api = environment.API_SELLER_PENDING_DISBURSMENT_REPORT + `?seller_id=${this.entityId}&settlement_date=${this.settlement_date}&npp=${environment.PAGE_LIMIT}`;
    this.details = [];
    this.getInfo(this.screen_api);
  }

  ngOnDestroy(){
    document.body.className="";
  }

  getInfo(urlPath) {
    $("#loader").show(250);
    this.list_status = false;
    // this.tbl_data = [];
    this.common.getInfo(urlPath).pipe(first()).subscribe((res: any) => {
      if(res.result.success) {
        const response = res.data;
        // const result = response.results;
        this.disb_data = response.results;
        this.data_length = this.disb_data.length;
        if ('cards' in  response) {
          const cards = response.cards;
          this.card_data = cards;
        }
        const pagination = res.data.pagination;
        this.tbl_pagination = pagination;
        this.p = pagination.current;
        this.totalItems = pagination.total;
        if (pagination.total <= 10) { //make it 10
          this.pagination_length = false;
        }
      }
      this.list_status = true;
      $("#loader").hide(250);
    }, error => { localStorage.clear(); location.reload(); });
  }

  setPrimaryKey(objectData: object) {
    for (const key of Object.keys(objectData)) {
      if (key === this.primary_key) {
        return objectData[key];
      }
    }
  }

  getDetailedData(item){
    this.details.push(item);
    this.list = false;
    this.showDetails = true;
    this.listStatus = true;
    this.shared.nextDisbItem(this.details);
    this.router.navigate(['/pendingdisbursementdetails']);

  }

  getPage(page: number) {
    $("#loader").show(250);
    this.p = page;
    this.getPageCounter({currentObj: page, pageLimit: this.pageLimit});
  }
  pageCounter(page, num) {
    if (page > 0) {
      let current = page - 1;
      current = current * this.pageLimit + num;
      return current;
    } else {
      return num;
    }
  }
  getPageCounter(pageObject: any) {
    $("#loader").show(250);
    const pagePath = this.screen_api + `&page=${pageObject.currentObj}`;
    this.getInfo(pagePath);
  }

  showLast(start, length) {
    return start + (length - 1);
  }


}
