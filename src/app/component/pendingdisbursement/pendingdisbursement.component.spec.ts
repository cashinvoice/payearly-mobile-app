import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingdisbursementComponent } from './pendingdisbursement.component';

describe('PendingdisbursementComponent', () => {
  let component: PendingdisbursementComponent;
  let fixture: ComponentFixture<PendingdisbursementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingdisbursementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingdisbursementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
