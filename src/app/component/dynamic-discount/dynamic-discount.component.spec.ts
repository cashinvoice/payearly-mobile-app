import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicDiscountComponent } from './dynamic-discount.component';

describe('DynamicDiscountComponent', () => {
  let component: DynamicDiscountComponent;
  let fixture: ComponentFixture<DynamicDiscountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicDiscountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicDiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
