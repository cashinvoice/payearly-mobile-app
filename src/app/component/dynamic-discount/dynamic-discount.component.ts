import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "../../_service/authentication.service";
import { GlobalFunctionService } from "../../_service/global-function.service";
import { CommonService } from "../../_service/common.service";
import { SharedService } from "../../_service/shared.service";

import { NgxSpinnerService } from "ngx-spinner";

import { environment } from "../../../environments/environment";
import { first } from "rxjs/operators";
import { Router } from "@angular/router";
import {
  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  ValidatorFn,
} from "@angular/forms";
import { of } from "rxjs";

declare var $: any;
declare var jQuery: any;
declare const toastbox: any;

@Component({
  selector: 'app-dynamic-discount',
  templateUrl: './dynamic-discount.component.html',
  styleUrls: ['./dynamic-discount.component.css']
})
export class DynamicDiscountComponent implements OnInit {

  entity_id = localStorage.getItem('entity_id');
  product_id = localStorage.getItem('product_id');
  settlement_date = localStorage.getItem("settlement_date");
  header_title = "";
  data_length = 0;
  input_section = false;
  discount_list = false;
  discount_list_array = [];
  discount_selected = 0;
  m_cash_requirement = 0;
  m_discount_offered = 0;
  approved_item = 0;
  dynamic_interest_data = [];
  trnx_ids:any = [];
  max_value = 0;
  min_value = 0;
  steps_inc = 0;
  api_amount = 0;
  api_type = 0;
  list_status = false;
  cur_date = '';
  discPerc = 0;
  list = true;
  listStatus = false;


  pageLimit = environment.PAGE_LIMIT;
  p = 1;
  tbl_title = 'seller_transaction_report';
  tbl_data:any = [];
  pagination_length = true;
  tbl_pagination;
  pagination_data;
  totalItems = 0;
  tbl_length = 0;



  select_all = [];
  bulk_upload_status = false;
  card_data = [];
  showDetails = false;
  details: any = [];
  total_invoice_amount = 0;
  total_discount_amount = 0;
  total_net_amount = 0;
  total_approved_amount = 0;
  primary_key = "id";
  temp_item: any = {};


  calledonce = false;
  checkBtnShowFirstCall = false;
  checkBtnShowSecondCall = false;

  screenAPI = environment.API_SELLER_DYNAMIC_INTEREST;
  screen_api = environment.API_SELLER_DYNAMIC_DISCOUNTING_LIST;
  authorizeAPI = environment.API_BUYER_SELLER_CHECKER_AUTHORIZED_UPDATE;



  constructor( private router: Router,
    private auth: AuthenticationService,
    private global: GlobalFunctionService,
    private common: CommonService,
    private shared: SharedService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService) {
    this.auth.checklogin();
     }

  ngOnInit() {
    // $("#loader").show(0);

    this.shared.dynamicsharedItem.subscribe(item => this.details = item);
    this.shared.dynamiccashreqsharedItem.subscribe(item => this.m_cash_requirement = item);
    this.shared.dynamicdiscofferedsharedItem.subscribe(item => this.m_discount_offered = item);
    this.shared.dynamictbldatasharedItem.subscribe(item => this.tbl_data = item);
    this.shared.dynamicliststatussharedItem.subscribe(item => this.list_status = item);
    this.shared.dynamicpagesharedItem.subscribe(item => this.p = item);
    this.shared.dynamictotalsharedItem.subscribe(item => this.totalItems = item);
    this.shared.dynamictotalinvoiceItem.subscribe(item => this.total_invoice_amount = item);
    this.shared.dynamictotalapprovedItem.subscribe(item => this.total_approved_amount = item);

    document.body.className = "backgroundImg";

    if (!this.tbl_data) {
      this.data_length = 0;
    } else {
      this.data_length = this.tbl_data.length;
    }

    // this.screen_api = environment.API_SELLER_DYNAMIC_DISCOUNTING_LIST
    let urlpath = environment.API_BUYER_SELLER_CHECKER_AUTHORIZED;
    const body = document.getElementsByTagName("body")[0];
    body.classList.remove("login");
    body.classList.add("nav-md");
    const url_name = this.router.url;
    this.header_title = url_name.replace("/", "");
    this.global.setTitle(this.header_title);
    let discURL = this.screenAPI + `?entity_id=${this.entity_id}&product_id=${this.product_id}`;
    if (!this.list_status) {
      this.getDiscInfo(discURL);
    } else{
      $("#loader").hide(250);
    }
  }

  getDiscInfo(urlPath) {
    $("#loader").show(0);
    this.common.getInfo(urlPath).pipe(first()).subscribe((res: any) => {
      if(res.result.success) {
        // if(!this.card_data){
          const response = res.data;
          this.dynamic_interest_data = response;
          let int = response[response.length-1];
          // this.min_value = response[0];
          this.max_value = int;
          // this.steps_inc = Number(response[1]) - Number(response[0]);
          this.m_discount_offered = this.max_value;
          // }
      }
      // if (!this.list_status) {
        this.dynamicDiscounting(this.max_value, 102, '');
      // }
    }, error => { localStorage.clear(); location.reload(); });
  }

  dynamicDiscounting(amount, type, trnx_id) {
// debugger
    this.api_amount = amount;
    this.api_type = type;
    this.list_status = false;
    this.shared.nextDynamicliststatusItem(this.list_status);
    this.data_length = 0;
    $("#loader").show(250);
    let temp_obj:any = {};
    // this.list_status = false;
    // this.dynamic_status = false;
    let url = this.screen_api;
    if (type == 101) {
      // url += `&amt_required=${amount}`;
      temp_obj.seller_id = this.entity_id;
      if(typeof(amount) == 'string'){
      amount = amount.replace(/,/g, "");
      amount = amount.replace(/₹/g, "");
      }
      temp_obj.amt_required = amount;
      temp_obj.trnx_id = trnx_id;
      temp_obj.product_id = this.product_id;
    } else {
      // url += `&int_selected=${amount}`;
      temp_obj.seller_id = this.entity_id;
      temp_obj.int_selected = amount;
      temp_obj.trnx_id = trnx_id;
      temp_obj.product_id = this.product_id;
    }
    this.common.postInfo(url, temp_obj).pipe(first()).subscribe((res: any) => {
      if(res.result.success) {
        const response = res.data;
        const result = response[0];
        if('intApplicable' in result){
        this.discPerc = result.intApplicable;
        }
        const invoices = result.invoices;
        if ('cur_date' in result) {
          this.cur_date = result.cur_date;
        }
        // this.cur_date = response.cur_date;
        let temp_card: any = {};

        // const pagination = res.data.pagination;
        // this.tbl_pagination = pagination;
        // this.p = pagination.current;
        this.totalItems = invoices.length;
        temp_card.totalAmountApproved = result.totalAmoutApproved;
        temp_card.totalInvoice = result.totalInvoice;
        this.total_approved_amount = result.totalAmoutApproved;
        this.total_invoice_amount = result.totalInvoice;
        this.m_cash_requirement = result.totalInvoice;
        this.m_discount_offered = result.intApplicable;
        this.shared.nextDynamiccashreqItem(this.m_cash_requirement);
        this.shared.nextDynamicdiscofferedItem(this.m_discount_offered);
        this.shared.nextDynamictbldataItem(invoices);
        this.shared.nextDynamictotalItem(this.totalItems);
        this.shared.nextDynamictotalamountItem(this.total_approved_amount);
        this.shared.nextDynamictotalapprovedItem(this.total_approved_amount);
        temp_card.discAmt = result.discAmt;
        if (type == 101) {
          temp_card.amount = Number(amount);
        } else {
          temp_card.amount = 0;
        this.m_cash_requirement = 0;

        }
        temp_card.intApplicable = result.intApplicable;
        temp_card.intrestRange = result.intrestRange;
        // this.card_data.total_amt = result.totalInvoice;
        // this.card_data.totalAmoutApproved = result.totalAmoutApproved;
        // this.dynamic_data = temp_card;
        this.tbl_data = invoices;
        this.data_length = this.tbl_data.length;
        this.card_data.push(temp_card);
        // this.total_approved_amount = this.card_data[0].totalAmountApproved;

        // this.dynamic_status = true;
      } else {
        // this.dynamic_status = false;
      }
      this.list_status = true;
      this.shared.nextDynamicliststatusItem(this.list_status);
      $("#loader").hide(250);
        });
    if(this.checkBtnShowFirstCall==false){
      this.checkBtnShowFirstCall = true;
    }else{
      this.checkBtnShowSecondCall = true;
    }
    // this.pending_acc_list = false;
  }



  getDetailedData(item) {
    this.select_all = [];
    this.details = [];
    this.details.push(item);
    this.details.push(this.cur_date);
    this.details.push(this.discPerc);
    this.list = false;
    this.showDetails = true;
    this.listStatus = true;
    this.shared.nextDynamicItem(this.details);
    this.router.navigate(["/dynamicdiscountdetails"]);
  }



  selectAll(event) {
    const tableList = JSON.parse(JSON.stringify(this.tbl_data));
    this.select_all = [];
    this.trnx_ids = [];
    if (event.target.checked == true) {
      // this.total_invoice_amount = 0;
      this.total_discount_amount = 0;
      this.total_net_amount = 0;
      this.total_approved_amount = 0;
      let i;
      for (i = 0; i < tableList.length; i++) {
        const rowUniqueId =
          "#action-checkbox-" + this.setPrimaryKey(tableList[i]);
        jQuery(rowUniqueId).prop("checked", true);
        const rowStatus = jQuery(rowUniqueId).prop("checked");
        if (rowStatus) {
          let send_obj: any = {};
          let newItem: any = {};
          let itemArr: any = [];
          newItem.buyer_id = tableList[i].buyer_id;
          newItem.seller_id = tableList[i].seller_id;
          newItem.product_id = this.product_id;
          newItem.id = tableList[i].id;
          newItem.status = tableList[i].status;
          newItem.invoice_due_date = tableList[i].invoice_due_date;
          newItem.disb_date = this.settlement_date;
          newItem.disb_amt = tableList[i].net_payable_amt;
          newItem.disc_amt = tableList[i].disc_amt;
          newItem.agreed_discount_percentage =
          tableList[i].agreed_discount_percentage;
          this.trnx_ids.push(tableList[i].id);
          // this.total_invoice_amount += Number(tableList[i].inv_amt);
          // this.total_discount_amount += Number(tableList[i].disc_amt);
          // this.total_net_amount += Number(tableList[i].net_payable_amt);
          this.m_cash_requirement += Number(tableList[i].inv_amt);
          this.total_approved_amount += Number(tableList[i].inv_amt);




          // itemArr.push(newItem);
          // send_obj.transactions = itemArr;
          this.select_all.push(newItem);
          // this.select_all.push(tableList[i]);
        }
      }
    } else {
      this.select_all = [];
      this.trnx_ids = [];
      // this.total_invoice_amount = Number(this.card_data[0].total_amt);
      // this.total_discount_amount = Number(this.card_data[0].disc_amt);
      // this.total_net_amount = Number(this.card_data[0].total_amt - this.card_data[0].disc_amt);
      this.m_cash_requirement = 0;
      this.total_approved_amount = this.card_data[0].totalAmountApproved;
      jQuery(".tbody_check_content").prop("checked", false);
    }
    this.applyScript();
  }

  single_select(event, rowObject: Object) {
    const rowData = JSON.parse(JSON.stringify(rowObject));
    if (event.target.checked === true) {
      const temp_child: any = {};
      const rowUniqueId = "#action-checkbox-" + this.setPrimaryKey(rowData);
      $(rowUniqueId).prop("checked", true);
      let send_obj: any = {};
      let newItem: any = {};
      let itemArr: any = [];
      newItem.buyer_id = rowData.buyer_id;
      newItem.seller_id = rowData.seller_id;
      newItem.product_id = this.product_id;
      newItem.id = rowData.id;
      newItem.status = rowData.status;
      newItem.invoice_due_date = rowData.invoice_due_date;
      newItem.disb_date = this.settlement_date;
      newItem.disb_amt = rowData.net_payable_amt;
      newItem.disc_amt = rowData.disc_amt;
      newItem.agreed_discount_percentage = rowData.agreed_discount_percentage;
      itemArr.push(newItem);
      this.trnx_ids.push(rowData.id);
      send_obj.transactions = itemArr;
      this.select_all.push(newItem);
      if (this.select_all.length == 1) {
        // this.total_invoice_amount = 0;
        // this.total_discount_amount = 0;
        // this.total_net_amount = 0;
        this.m_cash_requirement = 0;
        // this.card_data[0].totalAmountApproved = 0;
        this.total_approved_amount = 0;
      }

      // this.total_invoice_amount += Number(rowData.inv_amt);
      // this.total_discount_amount += Number(rowData.disc_amt);
      // this.total_net_amount += Number(rowData.net_payable_amt);
      this.m_cash_requirement += Number(rowData.inv_amt);
      this.total_approved_amount += Number(rowData.inv_amt);
    } else {

      const uniqueId = this.setPrimaryKey(rowData);
      const itemToDelete = [uniqueId];
      for (let i = 0; i < this.select_all.length; i++) {
        const obj = this.select_all[i];
        const removeId = this.setPrimaryKey(obj);
        if (itemToDelete.indexOf(removeId) !== -1) {
          // this.total_invoice_amount -= Number(rowData.inv_amt);
          // this.total_discount_amount -= Number(obj.disc_amt);
          // this.total_net_amount -= Number(obj.disb_amt);

          this.m_cash_requirement -= Number(rowData.inv_amt);
          this.total_approved_amount -= Number(rowData.inv_amt);

          this.select_all.splice(i, 1);
          this.trnx_ids.splice(i, 1);
          $("#action-checkbox-" + removeId).prop("checked", false);
          if(this.select_all.length == 0){
            this.m_cash_requirement -= Number(rowData.inv_amt);
            this.total_approved_amount -= Number(rowData.inv_amt);
          }
        }
        if (this.select_all.length == 0) {
          // this.total_invoice_amount += Number(this.card_data[0].total_amt);
          // this.total_discount_amount += Number(this.card_data[0].disc_amt);
          // this.total_net_amount += Number(this.card_data[0].total_amt - this.card_data[0].disc_amt);
          this.m_cash_requirement = 0;
          this.total_approved_amount = this.card_data[0].totalAmountApproved;
        }
      }
    }
    this.applyScript();
  }


  calculate(event){
    // debugger;
    event.preventDefault();
    this.dynamicDiscounting(this.m_cash_requirement, 101, this.trnx_ids);
    this.select_all = [];
    this.bulk_upload_status = false;
  }

  clearSelect() {
    this.select_all = [];
    this.temp_item = {};
    jQuery(".tbody_check_content").prop("checked", false);
  }

  authorized() {
    if (!jQuery('#customCheck5').is(':checked')) {
      // this.global.showError('Accept T&C');
      toastbox("accepttandc", 2000);
      return false;
    }
    let send_obj: any = {};
    send_obj.transactions = this.select_all;
    send_obj.type = "update";
    send_obj.postDate = this.cur_date;
    send_obj.categoryType = "dynamic";
    // send_obj.postDate = this.cur_date;
    send_obj.discPerc = this.discPerc;
    this.common
      .putInfo(this.authorizeAPI, send_obj)
      .pipe(first())
      .subscribe((res: any) => {
        // this.model_filter_status = false;
        // this.global.showSuccess('Authorized successfully submitted.')
        // this.getInfo(this.screen_api);
        if (res.result.success) {
          toastbox("toast-bulkauthorized-success", 5000);
          setTimeout(() => {
            window.location.reload();
          }, 500);
        } else {
          toastbox("toast-bulkauthorization-failure", 5000);
          setTimeout(() => {
            window.location.reload();
          }, 500);
        }
      });
  }

  authorizeSingle(item) {
    if (!jQuery('#customCheck6').is(':checked')) {
      // this.global.showError('Accept T&C');
      toastbox("accepttandc", 2000);
      return false;
    }
    let send_obj: any = {};
    let newItem: any = {};
    let itemArr: any = [];
    newItem.buyer_id = item.buyer_id;
    newItem.seller_id = item.seller_id;
    newItem.product_id = this.product_id;
    newItem.id = item.id;
    newItem.status = item.status;
    newItem.invoice_due_date = item.invoice_due_date;
    newItem.disb_date = this.settlement_date;
    newItem.disb_amt = item.net_payable_amt;
    newItem.disc_amt = item.disc_amt;
    newItem.agreed_discount_percentage = item.agreed_discount_percentage;
    itemArr.push(newItem);
    send_obj.transactions = itemArr;
    send_obj.type = "update";
    // send_obj.postDate = this.cur_date;

    send_obj.categoryType = "dynamic";
    send_obj.postDate = this.cur_date;
    send_obj.discPerc = this.discPerc;
    this.common
      .putInfo(this.authorizeAPI, send_obj)
      .pipe(first())
      .subscribe((res: any) => {
        // this.model_filter_status = false;
        // this.global.showSuccess('Authorized successfully submitted.')
        // this.getInfo(this.screen_api);
        if (res.result.success) {
          toastbox("toast-authorized-success", 5000);
          setTimeout(() => {
            // window.history.back();
          }, 500);
          setTimeout(() => {
            window.location.reload();
          }, 500);
        } else {
          toastbox("toast-authorization-failure", 5000);
        }
      });
    this.select_all = [];
    this.temp_item = {};
    jQuery(".tbody_check_content").prop("checked", false);
  }

  pushItem(event, item) {
    // event.stopPropagation();
    this.temp_item = item;
    setTimeout(function () {
      $("#DialogForSingle").modal("show");
    }, 0);

    event.stopPropagation();
  }

  setPrimaryKey(objectData: object) {
    for (const key of Object.keys(objectData)) {
      if (key === this.primary_key) {
        return objectData[key];
      }
    }
  }

  applyScript() {
    if (this.select_all.length > 1) {
      // jQuery('.footer_pagination_' + this.unique_key).css('pointer-events', 'none');
      this.bulk_upload_status = true;
    } else {
      // jQuery('.footer_pagination_' + this.unique_key).css('pointer-events', 'unset');
      this.bulk_upload_status = false;
    }
  }

  resetFilter(){
    this.shared.clearDynamicDiscData();
    location.reload();
  }



  getPage(page: number) {
    // $("#loader").show(250);
    this.p = page;
    this.shared.nextDynamicpageItem(this.p);
    this.getPageCounter({ currentObj: page, pageLimit: this.pageLimit });
  }

  pageCounter(page, num) {
    if (page > 0) {
      let current = page - 1;
      current = current * this.pageLimit + num;
      return current;
    } else {
      return num;
    }
  }


  showLast(start, length) {
    if(length-start<10) {
      return start + length - start;
    } else{
      return start + this.pageLimit - 1;
    }

  }


  getPageCounter(pageObject: any) {
    // $("#loader").show(250);
    // const pagePath = this.screen_api + `&page=${pageObject.currentObj}`;
    // this.dynamicDiscounting(this.api_amount, this.api_type, '', pagePath);
  }
}

