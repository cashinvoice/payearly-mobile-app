import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionreportdetailsComponent } from './transactionreportdetails.component';

describe('TransactionreportdetailsComponent', () => {
  let component: TransactionreportdetailsComponent;
  let fixture: ComponentFixture<TransactionreportdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionreportdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionreportdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
