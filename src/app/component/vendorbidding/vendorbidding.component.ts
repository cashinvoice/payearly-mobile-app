import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "../../_service/authentication.service";
import { GlobalFunctionService } from "../../_service/global-function.service";
import { CommonService } from "../../_service/common.service";
import { SharedService } from "../../_service/shared.service";
import { NgxSpinnerService } from "ngx-spinner";
import { environment } from "../../../environments/environment";
import { first } from "rxjs/operators";
import { Router } from "@angular/router";
import { of } from "rxjs";

declare var $: any;
declare var jQuery: any;
declare const toastbox: any;

@Component({
  selector: 'app-vendorbidding',
  templateUrl: './vendorbidding.component.html',
  styleUrls: ['./vendorbidding.component.css'],
})
export class VendorbiddingComponent implements OnInit {

  header_title = "";
  entity_id = localStorage.getItem("entity_id");
  is_financier = localStorage.getItem("is_financier");
  name = localStorage.getItem("name");
  roleName = localStorage.getItem("role_name");
  product_id = localStorage.getItem("product_id");
  entity_type = localStorage.getItem("entity_type");


  select_all = [];
  bulk_upload_status = false;
  primary_key = "id";
  data_length = 0;
  unique_key = crypto.getRandomValues(new Uint16Array(1))[0]*2;
  card_data = [];
  screen_api = "";

  showDetails = false;
  details: any = [];
  list = true;
  listStatus = false;

  authorizeAPI = environment.API_BUYER_SELLER_CHECKER_AUTHORIZED_UPDATE;
  pageLimit = 500;
  p = 1;
  totalItems;
  pagination_length = true;
  cur_date = '';

  tbl_data: any = [];
  tbl_pagination;

  total_invoice_amount = 0;
  total_discount_amount = 0;
  total_net_amount = 0;

  temp_item: any = {};




  debit_tbl_data: any = [];
  debit_data_length = 0;
  debit_tbl_pagination;
  debit_p = 1
  debitTotalItems;


  active_tab = 'vendorview_view';
  list_status_type = 12;

  vendor_api = '';
  debit_api = '';
  vendor_live = false;
  debit_notes_available = false;
  debit_amount = 0;
  bid_apr: any = {};
  m_bid = 0;
  bid_max_value = 0;
  bid_min_value = 0;
  btn_text = '';
  bid_steps_inc = 0;
  current_trend = 0;
  residual_content = false;

  constructor(private router: Router, private auth: AuthenticationService, private global: GlobalFunctionService, private common: CommonService, private shared: SharedService, private spinner: NgxSpinnerService) {
    this.auth.checklogin();
  }

  ngOnInit() {
    document.body.className = "backgroundImg";
    let url_name = this.router.url;
    if (url_name == '/vendorbidding/live') {
      this.vendor_live = true;
      // this.vendor_tbl_header.push({ name: 'Agreed Disc %'});
      // this.vendor_tbl_datatype_col.push({ name: 'agreed_discount_percentage', type: 'string'});
      // this.vendor_tbl_column.push('agreed_discount_percentage');
    } else {

      this.vendor_live = false;
    }
    // this.vendor_tbl_header.push({ name: 'tenor'});
    // this.vendor_tbl_datatype_col.push({ name: 'tenor', type: 'string'});
    // this.vendor_tbl_column.push('tenor');
    this.header_title = 'Vendor Biddding';

    this.global.setTitle(this.header_title);
    this.vendor_api = environment.API_VENDOR_BIDDING_DYNAMIC_DISCOUNT + `?entity_id=${this.entity_id}&npp=500`;
    if (this.vendor_live) {
      this.vendor_api = environment.API_VENDOR_BIDDING_DYNAMIC_DISCOUNT + `?mode=live&entity_id=${this.entity_id}&npp=500`;
    }
    this.getVendorInfo(this.vendor_api);

  }

  ngOnDestroy() {
    document.body.className = "";
  }

  getVendorInfo(api, obj?) {
    $("#loader").show(250);
    this.total_invoice_amount = 0;
    this.total_discount_amount = 0;
    this.total_net_amount = 0;
    this.common
      .postInfo(api, {})
      .pipe(first())
      .subscribe(
        (res: any) => {
          if (res.result.success) {
            const response = res.data;
            this.tbl_data = response.results;
            this.card_data = response.cards;
            this.cur_date = response.cur_date;

            this.data_length = this.tbl_data.length;
            const pagination = res.data.pagination;
            this.tbl_pagination = pagination;
            this.p = pagination.current;
            this.totalItems = pagination.total;
            this.total_invoice_amount += Number(this.card_data[0].total_amt);
            this.total_discount_amount += Number(this.card_data[0].disc_amt);
            this.total_net_amount += Number(this.card_data[0].total_amt - this.card_data[0].disc_amt);
            if ('debit_notes' in response && response.debit_notes != null) {
              this.debit_notes_available = true;
              this.debit_amount = response.debit_notes;
              if (this.debit_amount < 0) {
                this.debit_amount = -this.debit_amount;
              }
            }

            this.bid_apr.startThreshold = response.cards[0].startThreshold;
            this.bid_apr.endThreshold = response.cards[0].endThreshold;
            this.bid_apr.maxTrend = response.cards[0].maxTrend;
            this.m_bid = response.cards[0].endThreshold;
            this.bid_min_value = this.bid_apr.startThreshold;
            this.bid_max_value = this.bid_apr.endThreshold;
            this.current_trend = this.bid_apr.maxTrend;
            this.bid_steps_inc = 0.25;
            this.btn_text = 'Accept';
            if (pagination.total <= 10) {
              this.pagination_length = false;
            }
          }
          $("#loader").hide(250);
          setTimeout(() => {
            document.getElementById("allselect").click();
          }, 500);
        },
        (error) => {
          localStorage.clear();
          location.reload();
        }
      );
  }

  getDebitInfo(api, obj?) {
    $("#loader").show(250);
    this.common
      .getInfo(api)
      .pipe(first())
      .subscribe(
        (res: any) => {
          if (res.result.success) {
            const response = res.data;
            this.debit_tbl_data = response.results;
            this.debit_data_length = this.debit_tbl_data.length;
            const pagination = res.data.pagination;
            this.debit_tbl_pagination = pagination;
            // this.debit_p = pagination.current;
            // this.debitTotalItems = pagination.total;

            // if (pagination.total <= 10) {
            //   this.pagination_length = false;
            // }
          }
          $("#loader").hide(250);
        },
        (error) => {
          localStorage.clear();
          location.reload();
        }
      );
  }


  selectAll(event) {
    if (this.active_tab == 'debit_view'){

      var tableList = JSON.parse(JSON.stringify(this.debit_tbl_data));

    } else if (this.active_tab == 'vendor_view') {

      var tableList = JSON.parse(JSON.stringify(this.tbl_data));

    } else {

      var tableList = JSON.parse(JSON.stringify(this.tbl_data));
    }
    if (event.target.checked == true) {
      this.select_all = [];
      this.total_invoice_amount = 0;
      this.total_discount_amount = 0;
      this.total_net_amount = 0;
      let i;
      for (i = 0; i < tableList.length; i++) {
        const rowUniqueId = "#action-checkbox-" + this.setPrimaryKey(tableList[i]);
        debugger
        if ('residual_amt' in tableList[i] && tableList[i].residual_amt > 0) {
        if (!jQuery(`${rowUniqueId}`).is(':checked')) {
          jQuery(rowUniqueId).prop("checked", true);
          jQuery(`${rowUniqueId}`).prop("disabled", true);
        }
        this.residual_content = true;
      } else {
          jQuery(rowUniqueId).prop("checked", true);
          const rowStatus = jQuery(rowUniqueId).prop("checked");
          if (rowStatus) {
          let send_obj: any = {};
          let newItem: any = {};
          let itemArr: any = [];
          debugger
          newItem.buyer_id = tableList[i].buyer_id;
          newItem.seller_id = tableList[i].seller_id;
          newItem.product_id = this.product_id;
          newItem.id = tableList[i].id;
          newItem.status = tableList[i].status;
          newItem.invoice_due_date = tableList[i].invoice_due_date;
          newItem.disb_amt = tableList[i].net_payable_amt;
          newItem.disc_amt = tableList[i].disc_amt;
          newItem.inv_amt = tableList[i].inv_amt;
          newItem.tenor = tableList[i].tenor;
          // newItem.disc_amt = tableList[i].disc_amt;
          newItem.net_payable_amt = tableList[i].net_payable_amt;
          newItem.residual_amt = tableList[i].residual_amt;


          // newItem.agreed_discount_percentage =
          //   tableList[i].agreed_discount_percentage;

          this.total_invoice_amount += Number(tableList[i].inv_amt);
          this.total_discount_amount += Number(tableList[i].disc_amt);
          this.total_net_amount += Number(tableList[i].net_payable_amt);
          // itemArr.push(newItem);
          // send_obj.transactions = itemArr;
          this.select_all.push(tableList[i]);
          // this.select_all.push(tableList[i]);
        }

      }


      }
    } else {
      if (this.residual_content) {
        let tempArr = this.select_all;
        for (let i = 0; i < tableList.length; i++) {
          if ('residual_amt' in tableList[i] && tableList[i].residual_amt > 0) {
          } else {
            const rowUniqueId = '#action-checkbox-' + this.setPrimaryKey(tableList[i]);
            jQuery(rowUniqueId).prop('checked', false);
            }
          }
        this.select_all = this.select_all.filter( content => content.residual_amt > 0);
        if (this.select_all.length > 0){
          this.bulk_upload_status = true;
        } else {
          this.bulk_upload_status = false;
        }
      } else {
        this.select_all = [];
        this.total_invoice_amount = Number(this.card_data[0].total_amt);
        this.total_discount_amount = Number(this.card_data[0].disc_amt);
        this.total_net_amount = Number(this.card_data[0].total_amt - this.card_data[0].disc_amt);
        jQuery(".tbody_check_content").prop("checked", false); }

    }
    this.applyScript();
  }

  single_select(event, rowObject: Object) {
    const rowData = JSON.parse(JSON.stringify(rowObject));
    if (event.target.checked === true) {
      const temp_child: any = {};
      const rowUniqueId = "#action-checkbox-" + this.setPrimaryKey(rowData);
      $(rowUniqueId).prop("checked", true);
      let send_obj: any = {};
      let newItem: any = {};
      let itemArr: any = [];
      newItem.buyer_id = rowData.buyer_id;
      newItem.seller_id = rowData.seller_id;
      newItem.product_id = this.product_id;
      newItem.id = rowData.id;
      newItem.status = rowData.status;
      newItem.invoice_due_date = rowData.invoice_due_date;
      // newItem.disb_date = this.settlement_date;
      newItem.disb_amt = rowData.net_payable_amt;
      newItem.disc_amt = rowData.disc_amt;
      // newItem.agreed_discount_percentage = rowData.agreed_discount_percentage;
      itemArr.push(newItem);
      send_obj.transactions = itemArr;
      this.select_all.push(newItem);
      if (this.select_all.length == 1) {
        this.total_invoice_amount = 0;
        this.total_discount_amount = 0;
        this.total_net_amount = 0;
      }

      this.total_invoice_amount += Number(rowData.inv_amt);
      this.total_discount_amount += Number(rowData.disc_amt);
      this.total_net_amount += Number(rowData.net_payable_amt);
    } else {

      const uniqueId = this.setPrimaryKey(rowData);
      const itemToDelete = [uniqueId];
      for (let i = 0; i < this.select_all.length; i++) {
        const obj = this.select_all[i];
        const removeId = this.setPrimaryKey(obj);
        if (itemToDelete.indexOf(removeId) !== -1) {
          this.total_invoice_amount -= Number(rowData.inv_amt);
          this.total_discount_amount -= Number(obj.disc_amt);
          this.total_net_amount -= Number(obj.disb_amt);
          this.select_all.splice(i, 1);
          $("#action-checkbox-" + removeId).prop("checked", false);
          if (this.select_all.length == 0){
          }
        }
        if (this.select_all.length == 0) {
          this.total_invoice_amount += Number(this.card_data[0].total_amt);
          this.total_discount_amount += Number(this.card_data[0].disc_amt);
          this.total_net_amount += Number(this.card_data[0].total_amt - this.card_data[0].disc_amt);
        }
      }
    }
    this.applyScript();
  }



  setPrimaryKey(objectData: object) {
    for (const key of Object.keys(objectData)) {

      // if(this.active_tab == 'vendor_view'){

        if (key === this.primary_key) {
          return objectData[key];
        }
  
      // }

    }
  }

  applyScript() {
    if (this.select_all.length > 0) {
      // jQuery('.footer_pagination_' + this.unique_key).css('pointer-events', 'none');
      this.bulk_upload_status = true;
    } else {
      // jQuery('.footer_pagination_' + this.unique_key).css('pointer-events', 'unset');
      this.bulk_upload_status = false;
    }
  }

  getDetailedData(item) {
    this.select_all = [];
    this.details = [];
    this.details.push(item);
    this.details.push(this.cur_date);
    this.list = false;
    this.showDetails = true;
    this.listStatus = true;
    this.shared.nextItem(this.details);
    this.router.navigate(["/pendingacceptancedetails"]);
  }

  clearSelect() {
    this.select_all = [];
    this.temp_item = {};
    jQuery(".tbody_check_content").prop("checked", false);
  }

  authorized() {

    if (!jQuery('#customCheck2').is(':checked')) {
      // this.global.showError('Accept T&C');
      toastbox("accepttandc", 2000);
      return false;
    }
    let send_obj: any = {};
    send_obj.transactions = this.select_all;
    send_obj.type = "update";
    send_obj.categoryType = "dynamic";
    send_obj.postDate = this.cur_date;
    send_obj.discPerc = this.m_bid;
    this.common
      .putInfo(this.authorizeAPI, send_obj)
      .pipe(first())
      .subscribe((res: any) => {
        // this.model_filter_status = false;
        // this.global.showSuccess('Authorized successfully submitted.')
        // this.getInfo(this.screen_api);
        if (res.result.success) {
          toastbox("toast-bulkauthorized-success", 5000);
          setTimeout(() => {
            window.location.reload();
          }, 500);
        } else {
          toastbox("toast-bulkauthorization-failure", 5000);
          setTimeout(() => {
            window.location.reload();
          }, 500);
        }
      });
  }


  // withdraw(type) {
  //   if (type == 96) {
  //     this.spinner.show();
  //     let send_obj: any = {};
  //     send_obj.transactions = this.withdraw_data;
  //     send_obj.type = "update";
  //     send_obj.postDate = this.cur_date;
  //     this.common.putInfo(environment.API_UPDATE_WITHDRAW_BID, send_obj).pipe(first()).subscribe((res: any) => {
  //       // this.model_filter_status = false;
  //       // this.global.showSuccess('Withdrawl Successful');
  //       this.getVendorInfo(this.vendor_api);
  //     }, error => { localStorage.clear(); location.reload(); });
  //   } else {
  //     jQuery('#confirm_authorized').modal('hide');
  //   }
  // }

  // withdrawAll() {
  //     this.spinner.show();
  //     let send_obj: any = {};
  //     // send_obj.transactions = this.withdraw_data;
  //     send_obj.type = "update";
  //     send_obj.withdraw_type = 'all'
  //     send_obj.postDate = this.cur_date;
  //     send_obj.entity_id = this.entity_id;
  //     this.common.putInfo(environment.API_UPDATE_WITHDRAW_BID, send_obj).pipe(first()).subscribe((res: any) => {
  //       // this.model_filter_status = false;
  //       // this.global.showSuccess('Withdrawl Successful');
  //       this.getVendorInfo(this.vendor_api);
  //     }, error => { localStorage.clear(); location.reload(); });
  // }

  // authorizeSingle(item) {

  //   if (!jQuery('#customCheck3').is(':checked')) {
  //     // this.global.showError('Accept T&C');
  //     toastbox("accepttandc", 2000);
  //     return false;
  //   }

  //   let send_obj: any = {};
  //   let newItem: any = {};
  //   let itemArr: any = [];
  //   newItem.buyer_id = item.buyer_id;
  //   newItem.seller_id = item.seller_id;
  //   newItem.product_id = this.product_id;
  //   newItem.id = item.id;
  //   newItem.status = item.status;
  //   newItem.invoice_due_date = item.invoice_due_date;
  //   // newItem.disb_date = this.settlement_date;
  //   newItem.disb_amt = item.net_payable_amt;
  //   newItem.disc_amt = item.disc_amt;
  //   newItem.agreed_discount_percentage = item.agreed_discount_percentage;
  //   itemArr.push(newItem);
  //   send_obj.transactions = itemArr;
  //   send_obj.type = "update";
  //   send_obj.posrDate = this.cur_date;
  //   this.common
  //     .putInfo(this.authorizeAPI, send_obj)
  //     .pipe(first())
  //     .subscribe((res: any) => {
  //       // this.model_filter_status = false;
  //       // this.global.showSuccess('Authorized successfully submitted.')
  //       // this.getInfo(this.screen_api);
  //       if (res.result.success) {
  //         toastbox("toast-authorized-success", 5000);
  //         setTimeout(() => {
  //           // window.history.back();
  //         }, 500);
  //         setTimeout(() => {
  //           window.location.reload();
  //         }, 500);
  //       } else {
  //         toastbox("toast-authorization-failure", 5000);
  //       }
  //     });
  //   this.select_all = [];
  //   this.temp_item = {};
  //   jQuery(".tbody_check_content").prop("checked", false);
  // }

  pushItem(event, item) {
    // event.stopPropagation();
    this.temp_item = item;
    setTimeout(function () {
      $("#DialogForSingle").modal("show");
    }, 0);

    event.stopPropagation();
  }

  getPage(page: number) {
    $("#loader").show(250);
    this.p = page;
    // this.file_p = page;
    // this.vendor_p = page;
    this.getPageCounter({ currentObj: page, pageLimit: this.pageLimit });
  }

  pageCounter(page, num) {
    if (page > 0) {
      let current = page - 1;
      current = current * this.pageLimit + num;
      return current;
    } else {
      return num;
    }
  }

  showLast(start, length) {
    return start + (length - 1);
  }


  getPageCounter(pageObject: any) {
    $("#loader").show(250);
    const pagePath = this.screen_api + `&page=${pageObject.currentObj}`;
    this.getVendorInfo(pagePath);
  }


  refreshPage(){

    switch (this.active_tab) {
      case "vendor_view": this.vendorview_clicked();

                          break;

        case "debit_view": this.debitview_clicked();
                           break;
    }

  }

  debitview_clicked(){
    this.active_tab = 'debit_view';
    this.debit_api = environment.API_DEBIT_SELLER_LIST + `?entity_id=${this.entity_id}&npp=500`;
    this.getDebitInfo(this.debit_api);


  }

  vendorview_clicked(){
    // debugger
    this.active_tab = 'vendor_view';
    this.vendor_api = environment.API_VENDOR_BIDDING_DYNAMIC_DISCOUNT + `?entity_id=${this.entity_id}&npp=500`;
    if (this.vendor_live){
      this.vendor_api = environment.API_VENDOR_BIDDING_DYNAMIC_DISCOUNT + `?mode=live&entity_id=${this.entity_id}&npp=500`;
    }
    this.getVendorInfo(this.vendor_api);
  }

  // loadTransaction(rowObject: any) {
  //   this.transactionview_clicked(rowObject);
  // }

  
  changeBidApr(currentValue, type){
    if (currentValue < this.bid_max_value) {
    this.btn_text = 'Place Bid';
    } else {
    this.btn_text = 'Accept';
    }
    if (currentValue > this.bid_max_value || currentValue % this.bid_steps_inc != 0){
      
    } else if (currentValue < this.bid_min_value) {
      this.m_bid = this.bid_min_value;
      currentValue = this.bid_min_value;
    }
    for (let item of this.tbl_data) {
      item.disc_amt = Number((+(item.inv_amt) * +(item.tenor) * +(currentValue)) / 36500);
      item.net_payable_amt = Number(item.inv_amt - item.disc_amt); 
    }
    debugger
    this.total_invoice_amount = 0;
    this.total_discount_amount = 0;
    this.total_net_amount = 0;
    for (let item of this.select_all) {
      this.total_invoice_amount += item.inv_amt;
      this.total_discount_amount += Number((+(item.inv_amt) * +(item.tenor) * +(currentValue)) / 36500);
      this.total_net_amount += Number(item.inv_amt - Number((+(item.inv_amt) * +(item.tenor) * +(currentValue)) / 36500));

    }
  }

  checkLimit(value){

    if (value > this.bid_max_value || value % this.bid_steps_inc != 0){
      // this.m_bid = this.bid_max_value;
      // this.card_data[3].icon = 'fa-thumbs-up';


    } else if (value < this.bid_min_value) {
      this.m_bid = this.bid_min_value;
      // this.card_data[3].icon = 'fa-check';
    }
  }

}
