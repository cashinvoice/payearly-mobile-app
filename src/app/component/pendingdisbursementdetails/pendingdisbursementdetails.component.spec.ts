import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingdisbursementdetailsComponent } from './pendingdisbursementdetails.component';

describe('PendingdisbursementdetailsComponent', () => {
  let component: PendingdisbursementdetailsComponent;
  let fixture: ComponentFixture<PendingdisbursementdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingdisbursementdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingdisbursementdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
