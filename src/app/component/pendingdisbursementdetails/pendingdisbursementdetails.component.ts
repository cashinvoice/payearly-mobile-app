import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService } from '../../_service/authentication.service';
import { GlobalFunctionService } from '../../_service/global-function.service';
import { CommonService } from '../../_service/common.service';
import { SharedService } from '../../_service/shared.service';


import { NgxSpinnerService } from 'ngx-spinner';

import { environment } from '../../../environments/environment';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { of } from 'rxjs';


declare var $: any;
declare var jQuery: any;
declare const toastbox: any;

@Component({
  selector: 'app-pendingdisbursementdetails',
  templateUrl: './pendingdisbursementdetails.component.html',
  styleUrls: ['./pendingdisbursementdetails.component.css']
})
export class PendingdisbursementdetailsComponent implements OnInit {

  details = [];

  transaction_id: number;
  screen_api: string;
  list_status: boolean = false;

  constructor(private router: Router, private auth: AuthenticationService, private global: GlobalFunctionService, private common: CommonService, private shared: SharedService, private fb: FormBuilder, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    document.body.className = "backgroundImg";
    this.shared.clearSearchData();
    this.shared.clearDynamicDiscData();
    this.shared.disbsharedItem.subscribe(item => this.details = item);
    this.transaction_id = this.details[0].id;
    this.screen_api = environment.API_TRANSACTION_DETAILS + `?trnx_id=${this.transaction_id}`;
    this.getInfo(this.screen_api);
  }

  ngOnDestroy(){
    document.body.className="";
  }

  getInfo(urlPath) {
    $("#loader").show(250);
    this.list_status = false;
    this.common.getInfo(this.screen_api).pipe(first()).subscribe((res: any) => {
      if(res.result.success) {
        const response = res.data.invoice_details;
        this.details[0].buyer_gstn = response[0].buyer_gstn;
        this.details[0].seller_gstn = response[0].seller_gstn;
        this.details[0].invoice_adjustments = response[0].invoice_adjustments;
        this.details[0].invoice_net_amt = response[0].invoice_net_amt;
        this.details[0].invoice_acceptance_date = response[0].invoice_acceptance_date;
        this.details[0].buyer_ref_no = response[0].buyer_ref_no;
        this.details[0].po_number = response[0].po_number;
        }
      this.list_status = true;
      $("#loader").hide(250);
    }, error => { localStorage.clear(); location.reload(); });
  }

  goToList() {
    this.details = [];
    this.details.pop();
    this.router.navigate(['/pendingdisbursement']);
  }

}
