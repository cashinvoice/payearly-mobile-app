import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_service/authentication.service';
import { GlobalFunctionService } from '../../_service/global-function.service';
import { CommonService } from '../../_service/common.service';
import { SharedService } from '../../_service/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

import { environment } from '../../../environments/environment';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import {IMyDrpOptions} from 'mydaterangepicker';

declare var $: any;
declare var jQuery: any;
declare const toastbox: any;


@Component({
  selector: 'app-transactionreports',
  templateUrl: './transactionreports.component.html',
  styleUrls: ['./transactionreports.component.css']
})
export class TransactionreportsComponent implements OnInit {
  myDateRangePickerOptions: IMyDrpOptions = {
    dateFormat: 'dd.mm.yyyy',
  };

  public invoicenumModel: any = "";
  public invoiceModel: any = {};
  public disbursedModel: any = {};

  header_title = '';
  list_status = false;
  primary_key = 'id';
  tbl_title = 'seller_transaction_report';
  tbl_data:any = [];
  entity_id = localStorage.getItem('entity_id');
  screen_api = '';
  product_id  = localStorage.getItem('product_id');
  settlement_date = localStorage.getItem('settlement_date');
  filter_txt = '';
  filter_status = false;
  error_status = false;
  data_length = 0;
  details: any = [];
  showDetails = false;

  pageLimit = environment.PAGE_LIMIT;
  p = 1;
  totalItems;
  pagination_length = true;
  tbl_pagination;
  pagination_data;


  constructor(private formBuilder: FormBuilder, private common: CommonService, private shared: SharedService, private global: GlobalFunctionService, private spinner: NgxSpinnerService, private auth: AuthenticationService, private router: Router) {
    this.auth.checklogin();
   }

  ngOnInit() {
    document.body.className = "backgroundImg";
    this.shared.clearDynamicDiscData();
    if(this.tbl_data){
    this.shared.transactionsharedItem.subscribe(item => this.details = item);
    this.shared.invoicenumsharedItem.subscribe(item => this.invoicenumModel = item);
    this.shared.invoicesharedItem.subscribe(item => this.invoiceModel = item);
    this.shared.disbursedsharedItem.subscribe(item => this.disbursedModel = item);
    this.shared.tbl_datasharedItem.subscribe(item => this.tbl_data = item);
    this.shared.pagination_datasharedItem.subscribe(item => this.pagination_data = item);
    this.shared.list_statussharedItem.subscribe(item => this.list_status = item);
    this.p = this.pagination_data.current;
    this.totalItems = this.pagination_data.total;
    this.filter_status = true;

    }
    if(!this.tbl_data || this.tbl_data.length==0) {
      this.data_length = 0;
      this.filter_status = false;
      this.list_status = false;
    } else {
      this.data_length = this.tbl_data.length;
      this.list_status = true;
    }
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('login');
    body.classList.add('nav-md');
    let url_name = this.router.url;
    this.header_title = url_name.replace('/', '');
    this.global.setTitle(this.header_title);
    this.screen_api = environment.API_SELLER_REPORT_TRANSACTION_REPORT + `?seller_id=${this.entity_id}&settlement_date=${this.settlement_date}&npp=${environment.PAGE_LIMIT}`;
    $("#loader").hide(250);
  }

  ngOnDestroy(){
    document.body.className="";
  }

  getInfo(urlPath) {
    $("#loader").show(250);
    this.list_status = false;
    this.tbl_data = [];
    this.common.getInfo(urlPath).pipe(first()).subscribe((res: any) => {
      if(res.result.success) {
        const response = res.data;
        const result = response.results;
        this.tbl_data = result;
        this.data_length = this.tbl_data.length;
        this.pagination_data = res.data.pagination;
        const pagination = res.data.pagination;
        this.tbl_pagination = pagination;
        this.p = pagination.current;
        this.totalItems = pagination.total;
        //setting behavior subjects
        this.shared.nextInvoiceNumItem(this.invoicenumModel);
        this.shared.nextInvoiceItem(this.invoiceModel);
        this.shared.nextDisbursedItem(this.disbursedModel);
        this.shared.nextTblDataItem(this.tbl_data);
        this.shared.nextPaginationDataItem(this.pagination_data);
        if (pagination.total <= 10) {
          this.pagination_length = false;
        }else{
          this.pagination_length = true;
        }
      }
      else {
      }
      this.list_status = true;
      $("#loader").hide(250);
    }, error => { localStorage.clear(); location.reload(); });
  }

  searchKey(invoice_number, invoiceDate, disbursedDate) {
    this.data_length=0;
    let filter_txt = '';
     let invoice = invoice_number.trim();
     if(invoice.length > 0) {
       filter_txt +=  `&invoiceNo=${invoice_number}`;
     }

     let invoiceDateFilter = invoiceDate.selectionDayTxt;
     let disbursedDateinvoiceDateFilter = disbursedDate.selectionDayTxt;

     if (invoiceDateFilter.length > 1) {
       let split = invoiceDateFilter.split('-');
       let start_date = split[0].split('.');
       let end_date = split[1].split('.');
       filter_txt += `&invFromDate=${start_date[2].trim()}-${start_date[1].trim()}-${start_date[0].trim()}&invToDate=${end_date[2].trim()}-${end_date[1].trim()}-${end_date[0].trim()}`;
     }

     if (disbursedDateinvoiceDateFilter.length > 1) {
       let split = disbursedDateinvoiceDateFilter.split('-');
       let start_date = split[0].split('.');
       let end_date = split[1].split('.');
       filter_txt += `&disbFromDate=${start_date[2].trim()}-${start_date[1].trim()}-${start_date[0].trim()}&disbToDate=${end_date[2].trim()}-${end_date[1].trim()}-${end_date[0].trim()}`;
     }

     if(filter_txt.length > 0) {
       this.filter_status = true;
       this.filter_txt = filter_txt;
       this.getInfo(this.screen_api + this.filter_txt);
     } else {
      //  this.global.showError('Invalid parameters');
     }
   }

   resetFilter() {
    this.filter_status = false;
    this.filter_txt = ``;
    jQuery('#invoice_number').val('');
    jQuery('.selection').val('');
    this.list_status = false;
    this.shared.clearSearchData();

  }

  getReportDetails(event, item){
    this.details.push(item);
    // this.list = false;
    this.showDetails = true;
    this.list_status = false;
    this.shared.nextDisbItem(this.details);

    this.router.navigate(['/transactionreportdetails']);
    event.stopPropagation();
    $("#loader").hide(250);
  }

  showLast(start, length) {
    return start + (length - 1);
  }

  getPage(page: number) {
    this.p = page;
    this.getPageCounter({currentObj: page, pageLimit: this.pageLimit});
  }

  pageCounter(page, num) {
    if (page > 0) {
      let current = page - 1;
      current = current * this.pageLimit + num;
      return current;
    } else {
      return num;
    }
  }

  getPageCounter(pageObject: any) {
    let pagePath = this.screen_api+ this.filter_txt + `&page=${pageObject.currentObj}`;
    this.getInfo(pagePath);
  }



}
