import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingacceptancedetailsComponent } from './pendingacceptancedetails.component';

describe('PendingacceptancedetailsComponent', () => {
  let component: PendingacceptancedetailsComponent;
  let fixture: ComponentFixture<PendingacceptancedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingacceptancedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingacceptancedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
