import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_service/authentication.service';
import { GlobalFunctionService } from '../../_service/global-function.service';
import { CommonService } from '../../_service/common.service';

import { environment } from '../../../environments/environment';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Chart, ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import * as moment from 'moment';
import { Platform } from '@ionic/angular';

import { NgxSpinnerService } from "ngx-spinner";
import { SharedService } from 'src/app/_service/shared.service';
declare var $: any;
declare var jQuery: any;
Chart.defaults.global.defaultFontFamily = "'Poppins', sans-serif";
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  header_title = '';
  entityId = localStorage.getItem('entity_id');
  is_financier = localStorage.getItem('is_financier');
  name = localStorage.getItem('name');
  category = localStorage.getItem('category');
  card_data = [];
  cards:{['pending_acceptance']: any,['pending_acceptance_count']: any,['pending_disburment']: any,['pending_disburment_count']: any,['settled']: any,['settled_count']: any, ['avg_tenor']: any};
  buyer_checker_status: boolean;
  roleName = localStorage.getItem('role_name');
  settlement_date_one = localStorage.getItem('settlement_date_one');
  listStatus = false;

  chart_data = [];
  chart_status = false;

  chart_label = 'Settled Transaction';




  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{
      ticks: {
        fontSize: 10
    }
    }], yAxes: [{
      ticks: {
        fontSize: 10,
        callback: function(value, index, values) {
          return value + ' cr';
      }
    }
    }] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartData: ChartDataSets[] = [];
  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(0, 169, 85, 0.90)',
      borderColor: 'rgba(20, 220, 89,0.82)',
      pointBackgroundColor: 'rgba(20, 220, 89,0.82)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(20, 220, 89,0.82)'
      }];
  dashboardAPI = environment.API_SELLER_CHECKER_DASHBOARD;

  subscribe: any;

  constructor(
    private router: Router,
     private auth: AuthenticationService,
     private global: GlobalFunctionService,
     private common: CommonService,
     private shared: SharedService,
     private spinner: NgxSpinnerService,
     public platform: Platform) {
    this.auth.checklogin();

    this.subscribe = this.platform.backButton.subscribeWithPriority(666666, () => {
      if (this.constructor.name == "DashboardComponent"){
        if (window.confirm("Do you want to exit app?")) {
          navigator["app"].exitApp();
        }
      }
    });
  }

  ngOnInit() {
    document.body.className = "backgroundImg";
    this.shared.clearSearchData();
    this.shared.clearDynamicDiscData();
    let urlpath = environment.API_SELLER_CHECKER_DASHBOARD;
    const url_name = this.router.url;
    this.header_title = url_name.replace('/', '');
    this.global.setTitle(this.header_title);
      if ((localStorage.getItem('entity_type') && localStorage.getItem('role_name') && localStorage.getItem('product_id'))) {
        let roleName = localStorage.getItem('role_name');
        if(roleName != 'super_admin'){
        let product_id = Number(localStorage.getItem('product_id'));
        let entity_type = localStorage.getItem('entity_type');
        $("#loader").show(250);
        this.getDashInfo(urlpath+'?product_id='+product_id+'&entity_id='+this.entityId + '&settlement_one=' + this.settlement_date_one + '&limited=' + 'monthly');
      }
    }
}

ngOnDestroy(){
  document.body.className="";
}
// getInfo(api) {
//   this.common.getInfo(api).pipe(first()).subscribe((res: any) => {
//     if(res.result.success) {
//       const response = res.data;
//       this.card_data = response.cards;
//       this.spinner.hide();
//     }

//   }, error => { localStorage.clear(); location.reload(); });

// }

getDashInfo(urlpath) {
  let obj = {};
  this.common.getInfo(urlpath).pipe(first()).subscribe((res: any) => {
    if(res.result.success) {
      // const response = res.data;
      // this.card_data = res.data;
      obj['pending_acceptance'] = res.data.pending_acceptance;
      obj['pending_acceptance_count'] = res.data.pending_acceptance_count;
      obj['pending_disburment'] = res.data.pending_disburment;
      obj['pending_disburment_count'] = res.data.pending_disbursment_count;
      obj['settled'] = res.data.settled;
      obj['settled_count'] = res.data.settled_count;
      obj['avg_tenor'] = res.data.avg_tenor;
      this.card_data.push(obj);
      this.cards = this.card_data[0];
      this.listStatus = true;
      // chart
      let settled = res.data.setteledChart;
      this.chart_data = [];
      if(settled.length) {
        let chartLevel = [];
        let chartData = [];
        let rowObject = settled;
        for (const column of Object.keys(rowObject)) {
          let col = rowObject[column];
          chartLevel.push(col.disbursed_date);
          chartData.push(col.trnx_amt);
        }
        chartLevel.reverse();
        chartData.reverse();

        let i;
        for (i = 0; i < chartData.length; i++) {
          this.chart_data.push({dt: chartLevel[i], amt: chartData[i]});
        }
        // this.chart_status = true;
      } else {
        this.chart_data = [
          {dt: moment().add(1, 'days').format('YYYY-MM-DD'), amt: 0},
        ];
        // this.chart_status = true;
      }
      
      let collection_array = [];
      if (this.chart_data.length > 0) {
        let filteredData = this.chart_data.filter((item) => item.amt != 0);
        if (filteredData.length > 0) {
        for (let key of Object.keys(this.chart_data)) {
          let collection = this.chart_data[key];
          this.barChartLabels.push(this.global.changeClientDateFormat(collection.dt));
          collection_array.push(collection.amt/10000000);
          this.barChartData = [{ data: collection_array, label: this.chart_label }];
        }
        this.chart_status = true;
      }
      $("#loader").hide(250);
    }
    $("#loader").hide(250);
  }
  }, error => { localStorage.clear(); location.reload(); });
}

}
