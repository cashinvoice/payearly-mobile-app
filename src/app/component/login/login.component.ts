import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { environment } from '../../../environments/environment';
import { AuthenticationService } from '../../_service/authentication.service';
import { GlobalFunctionService } from '../../_service/global-function.service';
import { first } from 'rxjs/operators';
import * as CryptoJS from 'crypto-js';
declare var $: any;
declare var jQuery: any;
declare const toastbox: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  intervalId: number = 0;
  valid_user: boolean = true;
  seconds: number = 30;
  loginForm: FormGroup;
  buyerId;
  sellerId;
  size: any = environment.GOOGLE_CAPTCHA_SIZE;
  lang: any = environment.GOOGLE_CAPTCHA_LANG;
  type: any = environment.GOOGLE_CAPTCHA_TYPE;
  siteKey: any = environment.GOOGLE_CAPTCHA;
  seceret: any = environment.SECERET_KEY_PASS;
  clickbutton = false;
  private returnUrl: any;
  private route: any;
  header_title = '';
  loginFailMsg = '';
  seller: boolean = true;
  otp: number;
  buyer_list: any = [];
  showOtpComponent = false;
  @ViewChild('ngOtpInput', { static: false}) ngOtpInput: any;
  config = {
    allowNumbersOnly: true,
    length: 4,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: '',
    inputStyles: {
      'width': '40px',
      'height': '40px',
      'border' : 'solid gray 2px'
    }
  };
  showLogindata = true;
  mobilenumber: number;
  otpbuttonDis = true;
  resendotp = true;
  otpclicked = false;

  constructor(private global: GlobalFunctionService, private formBuilder: FormBuilder, private auth: AuthenticationService, private routes: ActivatedRoute, private router: Router) {
    this.auth.checklogin();
  }

  ngOnInit() {
    $("#loader").hide(0);
    document.body.className = "loginbg";
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('login');
    // body.classList.remove('nav-md')
    // body.classList.remove('nav-sm')
    let url_name = this.router.url;
    // this.header_title = url_name.replace('/', '');
    // this.global.setTitle(this.header_title);
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      // recaptcha: ['', Validators.required]
    });
  //   $(document).ready(function () {
  //     setTimeout(() => {
  //         $("#loader").fadeToggle(250);
  //     }, 800); // hide delay when page load
  // });

  }

    ngOnDestroy(){
    document.body.className="";
  }


  get frmValidate() {
    return this.loginForm.controls;
  }
  onOtpChange(otp) {
    this.otp = otp;
    if(otp.replace(/[^0-9]/g,"").length === 4){
      this.otpbuttonDis = false;
    } else {
      this.otpbuttonDis = true;
    }
  }
  trimValue(formControl) {
     formControl.setValue(formControl.value.trim());
     }

  onSubmit(resend) {
    this.clickbutton = true;
    if (this.loginForm.invalid) {
      return;
    }
   $("#loader").show(0);
    let passwrd = CryptoJS.AES.encrypt(this.frmValidate.password.value, this.seceret.trim()).toString();
    let resend_OTP ;
    resend_OTP = CryptoJS.AES.encrypt(resend,this.seceret.trim()).toString();
    this.auth.loginOTP(this.frmValidate.username.value,passwrd,resend_OTP).pipe(first())
      .subscribe(
        (res: any) => {
          $("#loader").hide(0);
          if (res.result.success) {
            if(res.data.otp_disabled == 1){
              this.otp = null;
             this.mobilenumber = null;
             this.loginAPI();
            } else {
              this.mobilenumber = res.data.mobile;
              this.showOtpComponent = true;
              this.showLogindata = false;
              this.countDown();
              this.resendotp = true;
          }
            } else {
              this.clickbutton = false;
              this.loginFailMsg = res.result.message;
              toastbox('toast-login', 5000);
            }
          },
          error => {
            this.clickbutton = false;
            localStorage.clear();
            location.reload();
          });
    }
    backToLogin(){
      this.clickbutton = false;
      this.showLogindata = true;
      this.showOtpComponent = false;
      this.clearTimer();
      this.seconds = 30;
    }
    clearTimer(): void { clearInterval(this.intervalId); }
    countDown(): void {
    // this.clearTimer();
    this.intervalId = window.setInterval(() => {
      this.seconds -= 1;
      if (this.seconds === 0) {
        this.resendotp = false;
        this.clearTimer();
      } else {
        if (this.seconds < 0) { this.seconds = 30; } // reset
      }
    }, 1000);
  }
  localStoragedata(user){
    if (user.result.success) {
      const respContent = user.data;
      if(respContent){
        localStorage.setItem('access_token', respContent.token);
        /* localStorage.setItem('name', respContent.user_name); */
        localStorage.setItem('email_id', respContent.user_email);
        localStorage.setItem('designation', respContent.designation);
        localStorage.setItem('name', respContent.name);
        localStorage.setItem('is_financier', respContent.is_financier);
        localStorage.setItem('entity_id', respContent.entity_id);
        if('is_approval' in respContent) {
          localStorage.setItem('is_approval', respContent.is_approval);
        }
        if ('entity_type' in respContent) {
          localStorage.setItem('entity_type', respContent.entity_type);
          localStorage.setItem('role_name', respContent.role_name);
          localStorage.setItem('product_id', respContent.product_id);
        }
        if ('settlement_date' in respContent) {
          localStorage.setItem('settlement_date', respContent.settlement_date);
        }
        if ('category' in respContent) {
          localStorage.setItem('category', respContent.category);
        }
        if ('settlement_date_one' in respContent) {
          localStorage.setItem('settlement_date_one', respContent.settlement_date_one);
        }
        if ('settlement_date_three' in respContent) {
          localStorage.setItem('settlement_date_three', respContent.settlement_date_three);
        }
        if ('category' in respContent) {
          localStorage.setItem('category', respContent.category);
        }
        if ('extended_period' in respContent) {
        localStorage.setItem('extended_period', respContent.extended_period);
        }
        if ('entity_name' in respContent) {
          localStorage.setItem('entity_name', respContent.entity_name);
        }
      } else{
        if(user.result.message == 'Mapping Not Found!'){
          this.loginFailMsg = "Login failed, No mapping found";
          alert(this.loginFailMsg)
        }
        
      }
      }
    return user;
  }
  close(){
    this.otpclicked = false;
  }
  onSubmitlogin() {
    this.otpclicked = true;
    if (this.otp) {
      $("#loader").show(0);
      let passwrd = CryptoJS.AES.encrypt(this.frmValidate.password.value, this.seceret.trim()).toString();
    this.auth.loginOtpVerify(this.frmValidate.username.value, passwrd,this.otp,
      this.mobilenumber).pipe(first()).subscribe(
          (res: any) => {
             $("#loader").hide(0);
            if (res.result.success) {
             this.loginAPI();
            } else {
              this.loginFailMsg = res.result.message;
              toastbox('toast-login', 5000);
              this.otpclicked = false;
            }
            });
      } else {
        this.otpbuttonDis = true;
        this.otpclicked = false;
      }
  }
  loginAPI() {
    $("#loader").show(0);
    // let passwd =  this.frmValidate.password.value;
    // let dsalt = this.captchaComponent.userEnteredCaptchaCode;
    // let passarr = CryptoJS.enc.Utf8.parse(dsalt+'~~'+passwd);
    // let passString = CryptoJS.enc.Base64.stringify(passarr);
    let passwrd = CryptoJS.AES.encrypt(this.frmValidate.password.value, this.seceret.trim()).toString();
    this.auth.login(this.frmValidate.username.value, passwrd,this.otp, this.valid_user).pipe(first())
      .subscribe(
        (res: any) => {
           $("#loader").hide(0);
          if (res.result.success) {
            // this.countDown();
            if (Array.isArray(res.data)) {
              if (res.data.length > 1) {
                this.buyer_list = res.data;
                this.valid_user = false;
                jQuery('#sellerpopupdash').modal('show');
              } else {
                this.localStoragedata(res);
                this.router.navigate(['/dashboard']);
              }
            } else {
              if(Object.keys(res.data).length > 0){
              if (res.data.entity_type == 'seller') {
                if (res.data.buyerList[0].length > 1) {
                  this.buyer_list = res.data.buyerList[0];
                  jQuery('#buyerpopupdash').modal('show');
                } else {
                  this.localStoragedata(res);
                  this.router.navigate(['/dashboard']);
                }
              } else {
                if (res.data.entity_type == 'CI') {
                  this.localStoragedata(res);
                  this.router.navigate(['/entity']);
                } else {
                  this.localStoragedata(res);
                  this.router.navigate(['/dashboard']);
                }
              }
            } else {
            // this.global.showError(res.result.message);
            this.loginFailMsg = res.result.message;
            toastbox('toast-login', 5000);
            this.otpclicked = false;
          }
          } 
          } else {
            // this.global.showError(res.result.message);
            this.loginFailMsg = res.result.message;
            toastbox('toast-login', 5000);
          }
        },
        error => {
          localStorage.clear();
          location.reload();
        });
  }

setBuyer(buyer_id) {
  $("#loader").show(0);
  jQuery('#buyerpopupdash').modal('hide');
  let passwrd = CryptoJS.AES.encrypt(this.frmValidate.password.value, this.seceret.trim()).toString();
  this.auth.login(this.frmValidate.username.value, passwrd,this.otp, this.valid_user, buyer_id, this.sellerId, this.seller).pipe(first())
    .subscribe(
      (res: any) => {
        $("#loader").hide(0);
        if (res.result.success) {
          if(Object.keys(res.data).length > 0){
          if (res.data.entity_type == 'seller') {
            if (res.data.buyerList && res.data.buyerList) {
              if (res.data.buyerList[0].length > 1) {
                this.buyer_list = res.data.buyerList;
                jQuery('#buyerpopupdash').modal('show');
                this.seller = true;
                this.valid_user = false;
              } else {
                this.localStoragedata(res);
                this.router.navigate(['/dashboard']);
              }
            } else {
              this.localStoragedata(res);
              this.router.navigate(['/dashboard']);
            }
          } else {
            if (res.data.entity_type == 'CI') {
              this.localStoragedata(res);
              this.router.navigate(['/entity']);
            } else {
              this.localStoragedata(res);
              this.router.navigate(['/dashboard']);
            }
          }
        }  else {
          this.loginFailMsg = res.result.message;
          toastbox('toast-login', 5000);
          this.otpclicked = false;
        }
        } else {
          this.loginFailMsg = res.result.message;
          toastbox('toast-login', 5000);
          this.otpclicked = false;
        }
      },
      error => {
        localStorage.clear();
        location.reload();
      });
}

setSeller(asscociated_id, entity_type) {
  if (entity_type == "buyer") {
    this.seller = false;
    this.valid_user = true;
    this.buyerId = asscociated_id
  }
  else {
    this.seller = true;
    this.valid_user = false;
    this.sellerId = asscociated_id;
  }
  jQuery('#sellerpopupdash').modal('hide');
  // this.spinner.show();
  $("#loader").show(0);
  let passwrd = CryptoJS.AES.encrypt(this.frmValidate.password.value, this.seceret.trim()).toString()
  this.auth.login(this.frmValidate.username.value, passwrd,this.otp, this.valid_user, this.buyerId, this.sellerId, this.seller).pipe(first())
    .subscribe(
      (res: any) => {
        $("#loader").hide(0);
        if (res.result.success) {
          if(Object.keys(res.data).length > 0){
          this.localStoragedata(res)
          if (res.data.entity_type == 'seller') {
            if (res.data.buyerList && res.data.buyerList) {
              if (res.data.buyerList[0].length > 1) {
                this.buyer_list = res.data.buyerList[0];
                jQuery('#buyerpopupdash').modal('show');
                this.seller = true;
                this.valid_user = true;
              } else {
                this.localStoragedata(res);
                this.router.navigate(['/dashboard']);
              }
            }
          } else {
            if (res.data.entity_type == 'CI') {
              this.localStoragedata(res);
              this.router.navigate(['/entity']);
            } else {
              this.localStoragedata(res);
              this.router.navigate(['/dashboard']);
            }
          }
        } else{
           this.loginFailMsg = res.result.message;
          toastbox('toast-login', 5000);
          this.otpclicked = false;
        }
        } else {
          toastbox('toast-login', 5000);
        }
      },
      error => {
        localStorage.clear();
        location.reload();
      });
}

  showSuccess() {
    // this.global.showError('');
  }

}
