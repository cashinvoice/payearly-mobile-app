import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { environment } from '../../../environments/environment';
import { AuthenticationService } from '../../_service/authentication.service';
import { GlobalFunctionService } from '../../_service/global-function.service';
import { CommonService } from '../../_service/common.service';
import { first } from 'rxjs/operators';
declare var $: any;
declare var jQuery: any;
declare const toastbox: any;

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  screenAPI = '';
  loginForm: FormGroup;
  size: any = environment.GOOGLE_CAPTCHA_SIZE;
  lang: any = environment.GOOGLE_CAPTCHA_LANG;
  type: any = environment.GOOGLE_CAPTCHA_TYPE;
  siteKey: any = environment.GOOGLE_CAPTCHA;
  private returnUrl: any;
  private route: any;
  header_title = '';
  forgotPasswordFailMsg = '';
  forgotPasswordForm: FormGroup;
  constructor(private global: GlobalFunctionService, private common: CommonService, private formBuilder: FormBuilder, private auth: AuthenticationService, private routes: ActivatedRoute, private router: Router) {

  }

  ngOnInit() {
    document.body.className = "loginbg";
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('login');
    let url_name = this.router.url;
    this.header_title = url_name.replace('/', '');
    this.global.setTitle(this.header_title);
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });

     this.screenAPI = environment.API_RESET_PASSWORD_MAIL;
  }


    ngOnDestroy(){
    document.body.className="";
  }


  get frmValidate() {
    return this.forgotPasswordForm.controls;
  }
  onSubmit() {
    if (this.forgotPasswordForm.invalid) {
      return;
    }
    let tempObj: any = {};
    tempObj.email = this.frmValidate.email.value;

    this.common.postInfo(this.screenAPI, tempObj).pipe(first())
      .subscribe(
        (res: any) => {
          if (res.result.success) {
            toastbox('toast-emailsent', 5000);
            setTimeout(() => {
              this.router.navigate(['/login']);
            }, 6000);
            }else {
              this.forgotPasswordFailMsg = res.result.message;
              toastbox('toast-forgotpassfailure', 5000);
            }
          },
        error => {
          localStorage.clear();
          location.reload();
        });
  }

}
