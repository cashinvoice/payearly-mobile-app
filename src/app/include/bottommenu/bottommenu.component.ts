import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../_service/common.service';
import { SharedService } from '../../_service/shared.service';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-bottommenu',
  templateUrl: './bottommenu.component.html',
  styleUrls: ['./bottommenu.component.css']
})
export class BottommenuComponent implements OnInit {
  category = localStorage.getItem('category');

  constructor(private shared: SharedService, private router: Router) { }

  ngOnInit() {
  }

  initSubjects(){
    this.shared.nextInvoiceNumItem("");
    this.shared.nextInvoiceItem({});
    this.shared.nextDisbursedItem({});
    this.shared.nextTblDataItem([]);
    this.shared.nextPaginationDataItem([]);
  }

  redirectDashboard(){
    this.router.navigateByUrl("/");
  }

}
