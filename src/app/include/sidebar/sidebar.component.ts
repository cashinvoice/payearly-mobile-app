import { Component, OnInit } from '@angular/core';
import { AuthenticationService} from '../../_service/authentication.service';
import { GlobalFunctionService } from '../../_service/global-function.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
declare var $: any;
import { first } from 'rxjs/operators';
import { CommonService } from 'src/app/_service/common.service';
declare const toastbox: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  name = localStorage.getItem('name');
  is_financier = localStorage.getItem('is_financier');
  buyer_checker_status = false;
  roleName = '';
  entity_type = localStorage.getItem('entity_type');
  category = localStorage.getItem('category');
  loginFailMsg: any;

  constructor(
    private auth: AuthenticationService,
    private route: ActivatedRoute,
     private common: CommonService,
    private router: Router,
    public global: GlobalFunctionService) {

   }

  ngOnInit() {
    $(".modal-backdrop").remove();
  }

  logout() {
     this.common.postInfo(environment.API_LOGOUT,null).pipe(first()).subscribe((res: any) => {
    if (res.result.success) {
    this.auth.logout();
    $(".modal-backdrop").remove();
    // this.router.navigate(['/login']);
    } else{
      this.loginFailMsg = res.result.message;
            toastbox('toast-login', 5000);
    }
    });
  }

  pendingaccClicked(){
    $("#sidebarPanel").modal("hide");
    this.router.navigate(['/pendingacceptance']);
  }
  pendingdisbClicked(){
    $("#sidebarPanel").modal("hide");
    this.router.navigate(['/pendingdisbursement']);
  }
  dashboardClicked(){
    $("#sidebarPanel").modal("hide");
    this.router.navigate(['/dashboard']);
  }
  transreportClicked(){
    $("#sidebarPanel").modal("hide");
    this.router.navigate(['/transactionreports']);
  }
  dynamicdiscountClicked(){
    $("#sidebarPanel").modal("hide");
    this.router.navigate(['/dynamicdiscount']);
  }

}
