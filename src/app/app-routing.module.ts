import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { PendingacceptanceComponent } from './component/pendingacceptance/pendingacceptance.component';
import { PendingacceptancedetailsComponent } from './component/pendingacceptancedetails/pendingacceptancedetails.component';
import { PendingdisbursementComponent } from './component/pendingdisbursement/pendingdisbursement.component';
import { PendingdisbursementdetailsComponent } from './component/pendingdisbursementdetails/pendingdisbursementdetails.component';
import { TransactionreportsComponent } from './component/transactionreports/transactionreports.component';
import { TransactionreportdetailsComponent } from './component/transactionreportdetails/transactionreportdetails.component';
import { ForgotpasswordComponent } from './component/forgotpassword/forgotpassword.component';
import { DynamicDiscountComponent } from './component/dynamic-discount/dynamic-discount.component';
import { DynamicDiscountDetailsComponent } from './component/dynamic-discount-details/dynamic-discount-details.component';
import { PendingApprovalComponent } from './component/pending-approval/pending-approval.component';
import { VendorbiddingComponent } from './component/vendorbidding/vendorbidding.component';




const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'forgotpassword', component: ForgotpasswordComponent},
  { path: 'dashboard', component: DashboardComponent },
  { path: 'pendingacceptance', component: PendingacceptanceComponent },
  { path: 'pendingacceptancedetails', component: PendingacceptancedetailsComponent },
  { path: 'pendingdisbursement', component: PendingdisbursementComponent},
  { path: 'pendingdisbursementdetails', component: PendingdisbursementdetailsComponent},
  { path: 'transactionreports', component: TransactionreportsComponent},
  { path: 'transactionreportdetails', component: TransactionreportdetailsComponent},
  { path: 'dynamicdiscount', component: DynamicDiscountComponent},
  { path: 'dynamicdiscountdetails', component: DynamicDiscountDetailsComponent},
  { path: 'pendingapproval', component: PendingApprovalComponent},
  { path: 'vendorbidding', component: VendorbiddingComponent},
  { path: 'vendorbidding/live', component: VendorbiddingComponent},
  { path: '**', redirectTo: '' },

];
// const routes: Routes = [
//   {
//     path: 'home',
//     loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
//   },
//   {
//     path: '',
//     redirectTo: 'home',
//     pathMatch: 'full'
//   },
// ];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules,onSameUrlNavigation: 'reload' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const RoutingComponent = [
  DashboardComponent,
  LoginComponent,
  ForgotpasswordComponent,
  PendingacceptanceComponent,
  PendingacceptancedetailsComponent,
  PendingdisbursementComponent,
  PendingdisbursementdetailsComponent,
  TransactionreportsComponent,
  TransactionreportdetailsComponent,
  DynamicDiscountComponent,
  DynamicDiscountDetailsComponent,
  PendingApprovalComponent,
  VendorbiddingComponent,
];
