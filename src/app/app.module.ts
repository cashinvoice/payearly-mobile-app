import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NgOtpInputModule } from  'ng-otp-input';

import { AppComponent } from './app.component';
import { AppRoutingModule,RoutingComponent } from './app-routing.module';

import { environment } from '../environments/environment';
import { LoginComponent } from './component/login/login.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';

import { NgxSpinnerModule, NgxSpinnerService } from "ngx-spinner";

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { OwlModule } from 'ngx-owl-carousel';
import { SidebarComponent } from './include/sidebar/sidebar.component';
import { HeaderComponent } from './include/header/header.component';
import { BottommenuComponent } from './include/bottommenu/bottommenu.component';
import { PendingacceptanceComponent } from './component/pendingacceptance/pendingacceptance.component';
import { PendingacceptancedetailsComponent } from './component/pendingacceptancedetails/pendingacceptancedetails.component';
import { PendingdisbursementComponent } from './component/pendingdisbursement/pendingdisbursement.component';
import { PendingdisbursementdetailsComponent } from './component/pendingdisbursementdetails/pendingdisbursementdetails.component';
import { TransactionreportsComponent } from './component/transactionreports/transactionreports.component';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { TransactionreportdetailsComponent } from './component/transactionreportdetails/transactionreportdetails.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ShortNumberPipe } from './pipes/short-number.pipe';
import { IndianCurrencyPipe } from './pipes/indian-currency.pipe';
import { ChartsModule } from 'ng2-charts';
import { DatePipe } from './pipes/date-pipe.pipe';
import { ForgotpasswordComponent } from './component/forgotpassword/forgotpassword.component';
import { DynamicDiscountComponent } from './component/dynamic-discount/dynamic-discount.component';
import { DynamicDiscountDetailsComponent } from './component/dynamic-discount-details/dynamic-discount-details.component';
// import 'rxjs/add/operator/map'

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponent,
    LoginComponent,
    DashboardComponent,
    SidebarComponent,
    HeaderComponent,
    BottommenuComponent,
    PendingacceptanceComponent,
    PendingacceptancedetailsComponent,
    PendingdisbursementComponent,
    PendingdisbursementdetailsComponent,
    TransactionreportsComponent,
    TransactionreportdetailsComponent,
    ShortNumberPipe,
    IndianCurrencyPipe,
    DatePipe,
    ForgotpasswordComponent,
    DynamicDiscountComponent,
    DynamicDiscountDetailsComponent,
  ],
  entryComponents: [],
  imports: [
    NgOtpInputModule,
    BrowserModule,
    IonicModule.forRoot(), 
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    OwlModule,
    NgxSpinnerModule,
    MyDatePickerModule,
    MyDateRangePickerModule,
    NgxPaginationModule,
    ChartsModule,

  ],
  providers: [
    NgxSpinnerService,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}
