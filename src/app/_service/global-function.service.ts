import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../environments/environment';
// import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class GlobalFunctionService {

  constructor(private titleService: Title) { }

  setTitle(title: string) {
    let setTitle;
    if (title.trim().length > 0) {
      setTitle = title.charAt(0).toUpperCase() + title.slice(1);
    } else {
      setTitle = 'Home';
    }
    this.titleService.setTitle(`${setTitle} | ${environment.APP_TITLE}`);
  }

  changeClientDateFormat(str) {
    if (str == null) {
      return '';
    } else {
      if (str.length > 0) {
        const convert_date = str.split('-');
        return convert_date[2] + '-' + convert_date[1] + '-' + convert_date[0];
      } else {
        return '';
      }
    }

  }

  getToken() {
    return localStorage.getItem('access_token');
  }
}
