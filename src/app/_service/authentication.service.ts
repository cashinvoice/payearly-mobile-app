import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  baseurl = environment.API_ENDPOINT;
  constructor(private http: HttpClient, private router: Router) { }

  checklogin() {
    const tokens = localStorage.getItem('access_token');
    if (tokens == null || tokens == undefined || tokens == 'undefined' || !tokens) {
      this.router.navigate(['login']);
    } else {

      const currentUrl = this.router.url.split('?')[0];
      if (currentUrl === '/login') {
        this.router.navigate(['']);
      } else {
        let entity_type = localStorage.getItem('entity_type');
        if (entity_type == 'CI' && currentUrl == '/') {
          this.router.navigate(['/dashboard']);
        } else {
          return true;
        }
        /* if (entity_type == 'CI' && (currentUrl == '/entity' || currentUrl == '/onboard-entity' || currentUrl == '/relationship-list' || currentUrl == '/relationship' || currentUrl == '/leads/referred' || currentUrl == '/leads/approved' || currentUrl == '/leads/return' || currentUrl == '/leads/rejected' || currentUrl == '/leads/pending' || currentUrl == '/score/set-rule-master' || currentUrl == '/score/set-score-master' || currentUrl == '/uploadfiles' || currentUrl == '/score/calculator')) {

          return true;

        } else if (entity_type == 'financier' && (currentUrl == '/' || currentUrl == '/leads/referred' || currentUrl == '/leads/approved' || currentUrl == '/leads/return' || currentUrl == '/leads/rejected' || currentUrl == '/leads/pending' || currentUrl == '/score/set-rule-master' || currentUrl == '/score/set-score-master' || currentUrl == '/users' || currentUrl == '/score/calculator' || currentUrl == '/admin-reports')) {

          return true;

        } else if (entity_type == 'seller' && (currentUrl == '/' || currentUrl == '/checker-invoice' || currentUrl == '/previous-transaction' || currentUrl == '/open-transaction' || currentUrl == '/buyer' || currentUrl == '/buyer-summary' || currentUrl == '/seller/report/disbursment-report' || currentUrl == '/seller/report/pending-disbursal-report' || currentUrl == '/seller/report/overdue-report' || currentUrl == '/seller/report/repayment-report' || currentUrl == '/maker-invoice')) {

          return true;

        } else {

          if (entity_type == 'CI') {
            this.router.navigate(['/entity']);
          } else {
            this.router.navigate(['']);
          }

        } */

      }
    }
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['login']);
  }
  loginOTP(username: string, password: string, resend_OTP?: any) {
    return this.http.post<any>(this.baseurl + 'rest/setOtp ',
      { email_id: username, password: password,resend : resend_OTP})
      .pipe(map(user => {
        return user;
      }));
  }
  loginOtpVerify(username: string, password: string,otp: number,mobile: number, buyerId?: number,) {
    return this.http.post<any>(this.baseurl + 'rest/verifyOTP',
      { email_id: username, password: password, buyerId: buyerId,otp:otp,
        mobile:mobile})
      .pipe(map(user => {
        return user;
      }));
  }
  login(username: string, password: string,otp,valid_user:boolean, buyerId?: number,sellerId?:number,seller?:boolean) {
    return this.http.post<any>(this.baseurl + 'rest/login',
      { email_id: username, password: password,otp:otp,valid_user:valid_user, buyerId: buyerId,sellerId,seller:seller,
       })
      .pipe(map(user => {
        return user;
      }));
  }
}

