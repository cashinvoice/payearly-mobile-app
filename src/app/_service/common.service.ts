import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { GlobalFunctionService } from '../_service/global-function.service';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  baseurl = environment.API_ENDPOINT;
  NPP = environment.PAGE_LIMIT;
  seceret: any = environment.SECERET_KEY_PASS;


  constructor(private http: HttpClient, private router: Router, private global: GlobalFunctionService) { }

  getInfo(endpoint: string) {
    const token = this.global.getToken();
    const url = this.baseurl + endpoint;
    const headers = new HttpHeaders({ 'Authorization': 'Basic ' + token });
    endpoint = this.encryptData(endpoint);
    return this.http.get<any[]>(this.baseurl + endpoint, { headers: headers }).pipe(map(resp => {
      if (resp['result'].message != 'Token expired') {
        return resp;
      } else {
        let res = {
          result: {
            success: false
          }
        }
        localStorage.clear();
        this.router.navigate(['/login']);
        return res
      }
    }));
  }
  getFile(endpoint: string) {
    const token = this.global.getToken();
    const url = this.baseurl + endpoint;
    const headers = new HttpHeaders({'Authorization': 'Basic ' + token});
    endpoint = this.encryptData(endpoint);
    return this.http.get<any[]>(this.baseurl + endpoint,{headers: headers ,  responseType: 'blob' as 'json',observe: 'response'})
      .pipe(map(resp => {
      // return response;
      if (resp['result'].message != 'Token expired') {
        return resp;
      } else {
        let res = {
          result: {
            success: false
          }
        }
        localStorage.clear();
        this.router.navigate(['/login']);
        return res
      }
    }));
  }



  postInfo(endpoint: string, postObject: Object) {
    const token = this.global.getToken();
    const url = this.baseurl + endpoint;
    const headers = new HttpHeaders({'Authorization': 'Basic ' + token});
    endpoint = this.encryptData(endpoint);
    return this.http.post<any>(this.baseurl + endpoint, postObject, {headers: headers})
      .pipe(map(resp => {
        if (resp['result'].message != 'Token expired') {
        return resp;
        } else {
          let res = {
            result: {
              success: false
            }
          }
          localStorage.clear();
          this.router.navigate(['/login']);
          return res
        }
      }));
  }

  putInfo(endpoint: string, postObject: Object) {
    const token = this.global.getToken();
    const url = this.baseurl + endpoint;
    const headers = new HttpHeaders({'Authorization': 'Basic ' + token});
    endpoint = this.encryptData(endpoint);
    return this.http.put<any>(this.baseurl + endpoint, postObject, {headers: headers})
      .pipe(map(resp => {
        // return resp;
        if (resp['result'].message != 'Token expired') {
          return resp;
        } else {
          let res = {
            result: {
              success: false
            }
          }
          localStorage.clear();
          this.router.navigate(['/login']);
          return res
        }
      }));
  }

  updateInfo(endpoint: string, postObject: Object) {
    const token = this.global.getToken();
    const url = this.baseurl + endpoint;
    const headers = new HttpHeaders({'Authorization': 'Basic ' + token});
    endpoint = this.encryptData(endpoint);
    return this.http.patch<any>(this.baseurl + endpoint, postObject, {headers: headers})
      .pipe(map(resp => {
        // return resp;
        if (resp['result'].message != 'Token expired') {
          return resp;
        } else {
          let res = {
            result: {
              success: false
            }
          }
          localStorage.clear();
          this.router.navigate(['/login']);
          return res
        }
      }));
  }

  uploadFile(endpoint: string, formdata: any) {
    const token = this.global.getToken();
    const url = this.baseurl + endpoint;
    const headers = new HttpHeaders({ 'Authorization': 'Basic ' + token});
    endpoint = this.encryptData(endpoint);
    return this.http.post<any>(this.baseurl + endpoint, formdata, { headers: headers })
      .pipe(map(resp => {
        // return response;
        if (resp['result'].message != 'Token expired') {
          return resp;
        } else {
          let res = {
            result: {
              success: false
            }
          }
          localStorage.clear();
          this.router.navigate(['/login']);
          return res
        }
      }));
  }


  postInfoNoAuth(endpoint: string, postObject: Object) {
    const url = this.baseurl + endpoint;
    endpoint = this.encryptData(endpoint);
    return this.http.post<any>(this.baseurl + endpoint, postObject)
      .pipe(map(resp => {
        // return response;
        if (resp['result'].message != 'Token expired') {
          return resp;
        } else {
          let res = {
            result: {
              success: false
            }
          }
          localStorage.clear();
          this.router.navigate(['/login']);
          return res
        }
      }));
  }

  getInfoNoAuth(endpoint: string) {
    const url = this.baseurl + endpoint;
    endpoint = this.encryptData(endpoint);
    return this.http.get<any[]>(this.baseurl + endpoint).pipe(map(resp => {
        // return response;
        if (resp['result'].message != 'Token expired') {
          return resp;
        } else {
          let res = {
            result: {
              success: false
            }
          }
          localStorage.clear();
          this.router.navigate(['/login']);
          return res
        }
      }));
  }

  encryptData(endpoint){
    if(endpoint.includes('?')){
    let urlpoint = endpoint.split('?');
    let datdaenc = CryptoJS.AES.encrypt(urlpoint[1], this.seceret.trim()).toString()
    return  urlpoint[0] + '?s=' + encodeURIComponent(datdaenc);
    } else {
      return endpoint;
    }
}

}

