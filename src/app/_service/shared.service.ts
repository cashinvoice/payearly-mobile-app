import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private dynamicItem = new BehaviorSubject([]);
  dynamicsharedItem = this.dynamicItem.asObservable();

  private item = new BehaviorSubject([]);
  sharedItem = this.item.asObservable();

  private disbItem = new BehaviorSubject([]);
  disbsharedItem = this.disbItem.asObservable();

  private transactionItem = new BehaviorSubject([]);
  transactionsharedItem = this.transactionItem.asObservable();

  private invoicenumItem = new BehaviorSubject('');
  invoicenumsharedItem = this.invoicenumItem.asObservable();

  private invoiceItem = new BehaviorSubject({});
  invoicesharedItem = this.invoiceItem.asObservable();

  private disbursedItem = new BehaviorSubject({});
  disbursedsharedItem = this.disbursedItem.asObservable();

  private tbl_dataItem = new BehaviorSubject([]);
  tbl_datasharedItem = this.tbl_dataItem.asObservable();

  private pagination_dataItem = new BehaviorSubject([]);
  pagination_datasharedItem = this.pagination_dataItem.asObservable();

  private list_statusItem = new BehaviorSubject(false);
  list_statussharedItem = this.list_statusItem.asObservable();



  private cashreqItem = new BehaviorSubject(0);
  dynamiccashreqsharedItem = this.cashreqItem.asObservable();

  private discofferedItem = new BehaviorSubject(0);
  dynamicdiscofferedsharedItem = this.discofferedItem.asObservable();

  private tbldataItem = new BehaviorSubject([]);
  dynamictbldatasharedItem = this.tbldataItem.asObservable();

  private liststatusItem = new BehaviorSubject(false);
  dynamicliststatussharedItem = this.liststatusItem.asObservable();

  private pageItem = new BehaviorSubject(0);
  dynamicpagesharedItem = this.pageItem.asObservable();

  private totalItem = new BehaviorSubject(0);
  dynamictotalsharedItem = this.totalItem.asObservable();

  private totalinvoiceItem = new BehaviorSubject(0);
  dynamictotalinvoiceItem = this.totalinvoiceItem.asObservable();

  private totalapprovedItem = new BehaviorSubject(0);
  dynamictotalapprovedItem = this.totalapprovedItem.asObservable();




  constructor(private http: HttpClient, private router: Router) { }

  nextItem(item: []) {
    this.item.next(item);
  }
  nextDisbItem(item: []) {
    this.disbItem.next(item);
  }
  nextTransactionItem(item: []) {
    this.transactionItem.next(item);
  }

  nextInvoiceNumItem(item: '') {
    this.invoicenumItem.next(item);
  }

  nextInvoiceItem(item: {}) {
    this.invoiceItem.next(item);
  }

  nextDisbursedItem(item: {}) {
    this.disbursedItem.next(item);
  }

  nextTblDataItem(item: []) {
    this.tbl_dataItem.next(item);
  }

  nextPaginationDataItem(item: []) {
    this.pagination_dataItem.next(item);
  }

  nextListStatusItem(item: boolean) {
    this.list_statusItem.next(item);
  }




  nextDynamicItem(item:[]){
    this.dynamicItem.next(item);
  }

  nextDynamiccashreqItem(item: number) {
    this.cashreqItem.next(item);
  }
  nextDynamicdiscofferedItem(item: number) {
    this.discofferedItem.next(item);
  }
  nextDynamictbldataItem(item: []) {
    this.tbldataItem.next(item);
  }
  nextDynamicliststatusItem(item: boolean) {
    this.liststatusItem.next(item);
  }
  nextDynamicpageItem(item: number) {
    this.pageItem.next(item);
  }

  nextDynamictotalItem(item: number) {
    this.totalItem.next(item);
  }

  nextDynamictotalamountItem(item: number) {
    this.totalinvoiceItem.next(item);
  }

  nextDynamictotalapprovedItem(item: number) {
    this.totalapprovedItem.next(item);
  }

  clearSearchData(){
    this.nextInvoiceNumItem("");
    this.nextInvoiceItem({});
    this.nextDisbursedItem({});
    this.nextTblDataItem([]);
    this.nextPaginationDataItem([]);
    this.nextListStatusItem(false);
  }

  clearDynamicDiscData(){
    this.nextDynamicpageItem(0);
    this.nextDynamicItem([]);
    this.nextDynamiccashreqItem(0);
    this.nextDynamicdiscofferedItem(0);
    this.nextDynamicliststatusItem(false);
    this.nextDynamictbldataItem([]);
    this.nextDynamictotalItem(0);
    this.nextDynamictotalamountItem(0);
    this.nextDynamictotalapprovedItem(0);
  }
}
